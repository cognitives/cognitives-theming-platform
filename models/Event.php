<?php

namespace app\models;

use Yii;

/**
 * Description of Event
 *
 * @author Azam
 */
class Event extends BaseModel
{
    public static function getAllEvents()
    {
        $eventData = self::getData();
        return (isset($eventData['events'])) ? $eventData['events'] : [];
    }
    
    public static function getEventDetails($params)
    {
        $eventData = self::getData();
        if(isset($eventData['eventDetails']) && !empty($eventData['eventDetails'])) {
            foreach($eventData['eventDetails'] as $event) {
                if(isset($params['eventId']) && $event['eventGuid'] == $params['eventId']) {
                    return $event;
                }
            }
        }
       return [];
    }
}
