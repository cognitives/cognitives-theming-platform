<?php

namespace app\models;

use Yii;


/**
 * Description of User
 *
 * @author Azam
 */
class Notification extends BaseModel
{
    
    public static function getNotifications()
    {
        $data = self::getData();

        return isset($data['notifications']) ? $data['notifications']: [];
    }
}
