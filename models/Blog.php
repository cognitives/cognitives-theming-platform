<?php

namespace app\models;

use Yii;

/**
 * Description of Blog
 *
 * @author Rohit Gupta
 */
class Blog extends BaseModel
{
    public $urlFormat = 'absolute';
    public const TYPE_CHANNEL = 'channel'; 

    public static function getBlogByGuidAndOptionalNetworkId($blogGuid = NULL)
    {
        $xmlData = self::getData();
        $networkData = $xmlData['network'];
        foreach ($networkData['networkBlogs'] as $blog) {
            if (isset($blog['guid']) && $blog['guid'] == $blogGuid) {
                return $blog;
            }
            if ($blogGuid == NULL && isset($blog['is_default']) && $blog['is_default']) {
                return $blog;
            }
        }
        
        return [];
    }
    public static function getBlogByTitle($title){
        $xmlData = self::getData();
        $blogData = $xmlData['blogList'];
        foreach ($blogData as $blog) {
            if (isset($blog['title']) && $blog['title'] == $title) {
                return $blog;
            }
        }
        return [];
    }
    public static function getBlog($blogGuid = NULL)
    {
        $xmlData = self::getData();
        $networkData = $xmlData['blogList'];
        foreach ($networkData as $blog) {
            if (isset($blog['guid']) && $blog['guid'] == $blogGuid) {
                return $blog;
            }
        }

        return $networkData[0];
    }
    
    public static function getBlogOwner($blogId)
    {
        $xmlData = self::getData();
        if(isset($xmlData['users']['user'][0])) {
            $user = $xmlData['users']['user'][0];
            
            return [
                'id' => $user['id'],
                'guid' => $user['guid'],
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'username' => $user['username'],
                'email' => $user['email'],
                'bio' => $user['bio'],
                'display_name' => $user['display_name'],
                'cloudinary_public_id' =>  isset($user['media']['id']) ? $user['media']['id'] : '',
                'cloudinary_cloud_name' => isset($user['media']['cloudName']) ? $user['media']['cloudName'] : '',
                'cdn_path' => isset($user['media']['path']) ? $user['media']['path'] : '',
            ];
        }
        
        return NULL;
    }

    public static function getBlogByUrlPrefix($urlPrefix)
    {
        if(empty($urlPrefix)) {
            return false;
        }
        $xmlData = self::getData();
        $networkData = $xmlData['network'];
        $url = str_replace(['/', '@'], '', $urlPrefix);
        foreach ($networkData['networkBlogs'] as $blog) {
            $paramsArr = explode('@', $blog['link']);
            if (count($paramsArr) > 1 && trim($paramsArr[1]) == $urlPrefix) {
                return $blog['guid'];
            }
            if (count($paramsArr) > 1 && trim($paramsArr[1]) == $url) {
                return $blog['guid'];
            }            
            
            if ($blog['link'] == $urlPrefix) {
                return $blog['guid'];
            }
        } 
        
        return null;
    }

    public static function getHomeBlogSettings()
    {
        $blogData = self::getData();
        return isset($blogData['homeblogsetting']['blog_setting']) ? $blogData['homeblogsetting']['blog_setting']: [];
    }
    
    public static function getOwnerBlogs()
    {
        $blogData = self::getData();
        return (isset($blogData['ownerBlog'])) ? $blogData['ownerBlog'] : [];
    }
    
    public static function getBlogList($params = [])
    {
        $blogData = self::getData(); 
        $blogList = (isset($blogData['blogList'])) ? $blogData['blogList'] : [];

        if(isset($params['type']) && $params['type'] === NULL && count($params) == 0) {
            return $blogList;
        }
        if(count($blogList) > 0) {
            $i = 0;
            foreach($blogList as $list) {
                if(isset($params['type']) && $params['type'] !== NULL && $list['type'] != $params['type']) {
                    unset($blogList[$i]);
                }
                // if(isset($params['metaInfo']['show_home']) && $params['metaInfo']['show_home']  ){
                //     if( isset($list['additionalInfo']['show_home']) && $params['metaInfo']['show_home'] != $list['additionalInfo']['show_home'] )
                //         unset($blogList[$i]);
                // }
            }
        }
        
        return $blogList;
    }

    public function getBlogUrl($params = [])
    {
        $blogData = self::getData();
        $xmlData = self::getData();
        $networkData = $xmlData['network'];

        if(isset($params['urlFormat']) && $params['urlFormat'] == self::URL_FORMAT_RELATIVE) {
            $blogurl = !$blogData['is_default'] 
                    ? (($networkData['blog_url_format'] == Network::DEFAULT_BLOG_URL_FORMAT) ? '/@' . $blogData['url_prefix'] : '/' . $blogData['url_prefix'])
                    : '';
            if(isset($params['preview']) && $params['preview']) {
                $blogurl .= "?preview=1";
            }
            return $blogurl;
        }
    }
}
