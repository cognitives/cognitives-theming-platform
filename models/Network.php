<?php

namespace app\models;

use Yii;

/**
 * Description of Network
 *
 * @author Aman Dhaliwal
 */
class Network extends BaseModel
{
    const DEFAULT_BLOG_URL_FORMAT = 'normal';
    
    public static function getDefaultDomainByNetworkId($networkId, $expiryDate = NULL, $selCols = '*')
    {
        $networkData = self::getData();
        return [
            'network_id' => $networkData['network']['id'],
            'is_channeldomain' => 1,
            'domain_name' => $_SERVER['SERVER_NAME'],
            'is_namecheap' => 0,
            'expiry' => '2017-12-30',
            'is_external' => 0
        ];
    }
    
    public static function getNetworkData()
    {
        $xmlData = self::getData();
        return isset($xmlData['network']) ? $xmlData['network'] : NULL;
    }
    
    public static function getChannelData()
    {
        $xmlData = self::getData();
        return isset($xmlData['channel']) ? $xmlData['channel'] : NULL;
    }
}
