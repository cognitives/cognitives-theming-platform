<?php

namespace app\models;

use Yii;

/**
 * Description of Content Block
 *
 */
class ContentBlock extends BaseModel
{
    public $extendedAttribs;
    
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    const RECORDS_LIMIT = 20;
    
    public static function getAll()
    {
        $contentBlock = self::getData();
        if(isset($contentBlock['contentBlock']) && !empty($contentBlock['contentBlock'])) {
            return $contentBlock['contentBlock'];
        }
       return [];
    } 
    public static function get($contentBlockId)
    {
        $contentBlock = self::getData();
        if(isset($contentBlock['contentBlock']) && !empty($contentBlock['contentBlock'])) {
            foreach($contentBlock['contentBlock'] as $content) {
                if($contentBlock['id'] == $contentBlockId || $contentBlock['guid'] == $contentBlockId) {
                    return $content;
                }
            }
        }
        
       return array_shift($contentBlock['contentBlock']);
    }    
    public static function getKeywordsLinkedBlog($keyword,$blogId){
        $contentBlock = self::getData();
        if(isset($contentBlock['contentBlock']) && !empty($contentBlock['contentBlock'])) {
            foreach($contentBlock['contentBlock'] as $content) {
                if($contentBlock['keyword'] == $keyword || $contentBlock['blogId'] == $blogId) {
                    return $content;
                }
            }
        }
        return [];
    }
}

