<?php

namespace app\models;

use Yii;

/**
 * Description of SocialFeed
 *
 * @author Azam
 */
class SocialFeed extends BaseModel
{
    const SOURCE_VIMEO = 'vimeo';
    const SOURCE_TWITTER = 'twitter';
    const SOURCE_YOUTUBE = 'youtube';
    const SOURCE_FACEBOOK = 'facebook';
    const SOURCE_INSTAGRAM = 'instagram';
    const SOURCE_PINTEREST = 'pinterest';
    const SOURCE_GOOGLE = 'google';

    public static function getSocialFeed($options = [])
    {
        $socialFeedData = \app\models\BlogFeed::getBlogFeed(['is_default' => true]);
        foreach ($socialFeedData as $socialfeed) {
            if(isset($options['guid']) && $socialfeed['guid'] == $options['guid']) {
                return $socialfeed;
            }
        }
        
        $data = self::getData();
        return array_shift($data['socialArticleDetails']);
    }

}
