<?php

namespace app\models;

use Yii;

/**
 * Description of Article
 *
 * @author Aman Dhaliwal
 */
class Article extends BaseModel
{

    const STATUS_DRAFT = 'draft';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_PUBLISHED = 'published';
    const ARTICLE_LIMIT = 20;
    const ARTICLE_LIMIT_USER_PROFILE = 5;
    const DEFAULT_TYPE_ARTICLE = 'article';
    const DEFAULT_STATUS_DRAFT = 'draft';
    const ARTICLE_SOURCE_SYSTEM = 'system';
    const ARTICLE_SOURCE_RSS = 'rss';
    const TYPE_ARTICLE = 'article';
    const TYPE_EVENT = 'event';
    const IS_PROMOTED_YES = 1;
    const IS_PROMOTED_NO = 0;
    const INSIGHT_PROCESSED_NO = 0;
    const INSIGHT_PROCESSED_YES = 1;
    const ALSOPUBLISHIN_PROCESSED_NO = 0;
    const ALSOPUBLISHIN_PROCESSED_YES = 1;
    const RELATED_TYPE_DEFAULT = 'default';
    const RELATED_TYPE_ARTICLE = 'article';
    const RELATED_TYPE_TAGS = 'tags';

    public static function getArticle($articleId)
    {
        $articleData = self::getData();
        if(isset($articleData['articleDetails']) && !empty($articleData['articleDetails'])) {
            foreach($articleData['articleDetails'] as $article) {
                if(isset($article['id'])){
                    if($article['id'] == $articleId || $article['guid'] == $articleId) {
                        return $article;
                    }
                }
            }
        }
        
       return array_shift($articleData['articleDetails']);
    }

    public static function getRecentArticle()
    {
        $articleData = self::getData();
        if(isset($articleData['recentArticle']) && !empty($articleData['recentArticle'])) {
            return $articleData['recentArticle'];
        }
        
    }
    public static function getArticleBySlug($slug)
    {
        $articleData = self::getData();
        if(isset($articleData['articleDetails']) && !empty($articleData['articleDetails'])) {
            foreach($articleData['articleDetails'] as $article) {
                if($article["slug"] == $slug ) {
                    return $article;
                }
            }
        }
        return [];
    }
    public static function getPopularArticles($count = 3)
    {
        $articleData = self::getData();
        $articles = [];
        if(isset($articleData['popularArticle']) && count($articleData['popularArticle'])>0) {
            for($i=0; $i<$count; $i++) {
                if(!isset($articleData['popularArticle'][$i])) {
                    continue;
                }
                $articleData['popularArticle'][$i]['articleId'] = $articleData['popularArticle'][$i]['id'];
                $article = $articleData['popularArticle'][$i];
                $linkArr = parse_url($article['url']);
                if(isset($linkArr['path']) && !empty($linkArr['path'])) {
                    $article['url'] = $linkArr['path'];
                }
                $articles[] = $article;
            }
        }
        
        return $articles;
    }
    
    public static function getArticleMedia($articleGuid)
    {
        $articleData = self::getData();
        if(isset($articleData['popularArticle']) && count($articleData['popularArticle'])>0) {
            foreach($articleData['popularArticle'] as $article) {
                if($article['guid'] == $articleGuid || $article['id'] == $articleGuid) {
                    return $article['media'];
                }
            }
        }
        
        return [];
    }
    
    public static function getArticleUrl($params)
    {
        $articleData = self::getData();
        if(isset($articleData['popularArticle']) && count($articleData['popularArticle'])>0) {
            foreach($articleData['popularArticle'] as $article) {
                if($article['guid'] == $params['guid']) {
                    return $article['url'];
                }
            }
        }
        
        return '';
    }
    public static function hasVideoMedia($articleId){
        $articles = self::getArticleMedia($articleId) ;
        foreach($articles as $article){
            if( $article['type']=='video' ){
                return true;
            }
        }
        return false;
    }
    public static function videoMedia($articleId){
        $articlesMedia = self::getArticleMedia($articleId) ;
        $mediaArray=[];
        foreach($articlesMedia as $article){
            if( $article['type']=='video' ){
                $mediaArray[] = $articlesMedia;
            }
        }
        return $mediaArray;
    }

}
