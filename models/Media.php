<?php

namespace app\models;

use Yii;


/**
 * Description of Media
 *
 * @author Aman Dhaliwal
 */
class Media extends BaseModel
{
    public static function getMediaUrl($options)
    {
        if(isset($options['media']['cdn_path'])) {
            return $options['media']['cdn_path'];
        }
        return '';
    }
    public function get(){
        $mediaData = self::getData();
        return isset($mediaData['media']) ? $mediaData['media'] : [];
        
    }
    public function getMediaByGuid($mediaGuid){
        $mediaData = $this->get();
        foreach($mediaData as $media) {
            if($media['id'] == $mediaGuid || $media['guid'] == $mediaGuid) {
                return $media;
            }
        }
    }
}
