<?php

namespace app\models;

use Yii;

/**
 * Description of Menu
 *
 * @author Aman Dhaliwal
 */
class Menu extends BaseModel
{
    const MENU_TYPE_PAGE = 'page';
    const MENU_TYPE_BLOG = 'blog';
    const MENU_TYPE_CHANNEL = 'channel';
    
    const STATUS_ACTIVE_NO = 0;
    const STATUS_ACTIVE_YES = 1;
    
    const MENU_TYPE_HEADER = 'header';
    const MENU_TYPE_FOOTER = 'footer';
    
    public static function getMenu()
    {
        $menuData = self::getData();
        $menuItems = [];
        //Make menu items links relative
        if(isset($menuData['menu'])) {
            foreach($menuData['menu'] as $menu) {
                $linkArr = parse_url($menu['link']);
                if(isset($linkArr['path']) && !empty($linkArr['path'])) {
                    $menu['link'] = $linkArr['path'];
                }
                
                if(isset($menu['children']) && count($menu['children']) > 0) {
                    $children = [];
                    foreach($menu['children'] as $child) {
                        $linkChildArr = parse_url($child['link']);
                        if(isset($linkChildArr['path']) && !empty($linkChildArr['path'])) {
                            $child['link'] = $linkChildArr['path'];
                        }
                        $children[] = $child;
                    }
                    $menu['children'] = $children;
                }
                $menuItems[] = $menu;
            }
        }
        return $menuItems;
    }
}
