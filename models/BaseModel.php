<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;

/**
 * Description of BaseModel
 *
 * @author Pawan Kumar
 */
class BaseModel
{

    public static function getData()
    {
        $dataFilePath = \Yii::$app->params['dataFilesPath'] . '/' . \Yii::$app->params['dataFileName'];
        if (!file_exists($dataFilePath)) {
            throw new \yii\web\NotFoundHttpException('No data file found. Please export data to a json file from platform.');
        }
        try {
            $fileData = file_get_contents($dataFilePath);
            return \yii\helpers\BaseJson::decode($fileData);
        }
        catch(\Exception $ex) {
            throw new \yii\web\NotFoundHttpException('Data file is invalid. Please check your configuration for the data file. Error: ' . $ex->getMessage());
        }
    }

}
