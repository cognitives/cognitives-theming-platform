<?php

namespace app\models;

use Yii;

/**
 * Description of CallToAction
 *
 * @author Aman Dhaliwal
 */
class CallToAction extends BaseModel
{
    const TYPE_AD = 'ad';
    public static function getKeywordBasedCTA($keywords = NULL)
    {
        $ctaData = self::getData();
        
        return (isset($ctaData['calltoactions'])) ? array_shift($ctaData['calltoactions']) : [];
    }

}
