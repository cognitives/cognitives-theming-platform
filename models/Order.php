<?php

namespace app\models;

use Yii;


/**
 * Description of Order
 *
 * @author Ashish
 */
class Order extends BaseModel
{
    public static function getOrderData($params = [])
    {
        $data = self::getData();
        return isset($data['orders'])? $data['orders'] : [];
    }

}
