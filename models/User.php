<?php

namespace app\models;

use Yii;


/**
 * Description of User
 *
 * @author Azam
 */
class User extends BaseModel
{
    public $password;
    public $verifypassword;
    
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    const EMAIL_ADDRESS_VERIFIED = 1;
    
    const BYLINE_DEFAULT_VALUE = 'FirstNameLastName';
    const BYLINE_USERNAME = "Username";
    
    const DEVELOPER_MODE_DISABLED = 0;
    const DEVELOPER_MODE_ENABLED = 1;
        
    const USERS_LISTING_PER_PAGE_COUNT = 20;
    
    public static function getUsers($param = 'usersList' )
    {
        $userData = self::getData();
        if ($param != 'usersList' && $param == 'users') {
            return isset($userData['users']) ? $userData['users'] : [];
        }
        else{
            return isset($userData['usersList']) ? $userData['usersList'] : [];
        }
    }
    
    public static function getUserFollowers()
    {
        $userData = self::getData();
        
        return isset($userData['user_followers']) ? $userData['user_followers']: [];
    }
    
    public static function getUserFollowing()
    {
        $userData = self::getData();
        return isset($userData['user_following']) ? $userData['user_following']: [];
    }
    
    public static function getBlogsFollowedByUser()
    {
        $userData = self::getData();
        return isset($userData['user_follow_blogs']) ? $userData['user_follow_blogs']: [];
    }

    public static function getPopularUsersByViews($params = [])
    {
        $userData = self::getData();

        if(isset($userData['popularUsers']) && !empty($userData['popularUsers'])){
            $user =$userData['popularUsers'];
            return isset($user['byView']) ? $user['byView']: [];
        }
        return [];
    }
    public static function getPopularUsersByFollowers($params = [])
    {
        $userData = self::getData();
        if(isset($userData['popularUsers']) && !empty($userData['popularUsers'])){
            $user =$userData['popularUsers'];
            return isset($user['byFollowers']) ? $user['byFollowers']: [];
        }
        return [];
    }
    
    public static function getPopularUsersByPosts($params = [])
    {
        $userData = self::getData();
        if(isset($userData['popularUsers']) && !empty($userData['popularUsers'])){
            $user =$userData['popularUsers'];
            return isset($user['byPosts']) ? $user['byPosts']: [];
        }
        return [];
    }
    
    
    
}
