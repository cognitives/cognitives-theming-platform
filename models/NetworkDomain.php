<?php

namespace app\models;

use Yii;

/**
 * Description of NetworkDomain
 *
 * @author Aman Dhaliwal
 */
class NetworkDomain
{
    const  IS_CHANNEL_DOMAIN_YES = 1;
    const  IS_CHANNEL_DOMAIN_NO = 0;
    
    const  IS_DEFAULT_DOMAIN_YES = 1;
    const  IS_DEFAULT_DOMAIN_NO = 0;
    
    const  IS_NAMECHEAP_DOMAIN_YES = 1;
    const  IS_NAMECHEAP_DOMAIN_NO = 0;
    
    const  IS_EXTERNAL_DOMAIN_YES = 1;
    const  IS_EXTERNAL_DOMAIN_NO = 0;
    
    const  IS_NAMECHEAP_REGISTERED_YES = 1;
    const  IS_NAMECHEAP_REGISTERED_NO = 0;
    
}
