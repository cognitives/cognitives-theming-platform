<?php

namespace app\models;

use Yii;

/**
 * Description of Search
 *
 * @author Aman Dhaliwal
 */
class Search extends BaseModel
{
    public static function searchArticles()
    {
        $blogFeedData = self::getData();
        $data = [];
        if(isset($blogFeedData['blogfeed']) && count($blogFeedData['blogfeed']) > 0) {           
            foreach($blogFeedData['blogfeed'] as $article) {
                if(!strpos($article['url'],'/')) {
                    if(!empty($data)){
                        continue;
                    }
                    foreach($article['articles'] as $articleDetail) {
                        
                        if((isset($articleDetail['socialId']) && $articleDetail['socialId'] > 0) && (isset($articleDetail['articleId']) && empty($articleDetail['articleId']) )) {
                            continue;
                        }
                        
                        $data[] = $articleDetail;
                    }

                }
            }
        } 
        
        return $data;
    }
}
