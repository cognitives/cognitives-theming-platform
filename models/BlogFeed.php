<?php

namespace app\models;

use Yii;

/**
 * Description of BlogFeed
 *
 * @author Aman Dhaliwal
 */
class BlogFeed extends BaseModel
{
    public static function getBlogFeed($params = [])
    {
        $blogFeedData = self::getData();
        $articles = [];
        foreach($blogFeedData['blogfeed'] as $article) {
            if($article['url'] == Yii::$app->request->url) {
                return $article['articles'];
            }
            else {
                $path = parse_url($article['url'], PHP_URL_PATH);
                $path = (!empty($path)) ? $path : '/';
                $pathUrl=Yii::$app->request->url;
                $explodePathUrl = explode("/",$pathUrl);
                if($path == Yii::$app->request->url) {
                     return $article['articles'];
                }
                elseif(isset($explodePathUrl[1]) && !empty($path =='/'.$explodePathUrl[1]) ){
                    return $article['articles'];
                }
            }
            
            if(isset($params['is_default']) && $params['is_default'] && $article['url'] == '/') {
                return $article['articles'];            
            }
            if(isset($params['is_default']) && $params['is_default'] ) {
                return $article['articles'];            
            }
        }
        
        return $articles;
    }
    public static function getBlogFeedByUrl($params = [])
    {
        $blogFeedData = self::getData();
        $articles = [];
        foreach($blogFeedData['blogfeed'] as $article) {;            
            if(isset($params['url']) &&  $params['url'] == $article['url']) {
                return $article['articles'];            
            }
        }
        
        
        return $articles;
    }
}
