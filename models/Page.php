<?php

namespace app\models;

use Yii;

/**
 * Description of Article
 *
 * @author Azam
 */
class Page extends BaseModel
{
    const STATUS_ACTIVE_NO = 0;
    const STATUS_ACTIVE_YES = 1;
    const PAGE_LIMIT = 20;

    public static function getPageByGuid($guid)
    {
        $pageData = self::getData();
        
        if(isset($pageData['pages']) && count($pageData['pages']) > 0) {
            foreach($pageData['pages'] as $page) {
                if($page['guid'] == $guid){
                    return $page;
                }            
            }
        }
        
        return [];
    }
    
    public static function getPageData($slug)
    {
        $pageData = self::getData();
        
        foreach($pageData['pages'] as $page) {
            if($page['slug'] == $slug){
                return $page;
            }            
        }
        $page = array_shift($pageData['pages']);
                
        $page['title'] = ($slug == 'about-us') ? 'About Us' : $page['title'];
        $page['slug'] = $slug;
        $page['content'] ='<h1>Lorem ipsum dolor sit amet consectetuer adipiscing elit</h1><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa <strong>strong</strong>. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede <a class="external ext" href="#">link</a> mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu,consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.</p><h1>Lorem ipsum dolor sit amet onsectetuer adipiscing elit</h1><h2>Aenean commodo ligula eget dolor aenean massa</h2><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p><h2>Aenean commodo ligula eget dolor aenean massa</h2><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p><ul><li>Lorem ipsum dolor sit amet consectetuer.</li><li>Aenean commodo ligula eget dolor.</li>  <li>Aenean massa cum sociis natoque penatibus.</li></ul><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>';
        return $page;
    }

    public static function getPageDataByParams($params = [])
    {
        $xmlData = self::getData();
        $pages = $xmlData['pages'];
        $returnArr = [];

        if (!empty($params)) {
            foreach ($pages as $page) {
                if (isset($params['guid']) && $page['guid'] == $params['guid']) {
                    return $page;
                }
                if (isset($params['id']) && $page['id'] == $params['id']) {
                    return $page;
                }
                if (isset($params['status']) && $page['status'] == $params['status']) {
                    $returnArr[] = $page;
                }
            }
            return $returnArr;
        } else {
            if (!empty($pages)) {
                return $pages;
            }
        }
        return [];
    }
}
