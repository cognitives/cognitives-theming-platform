<?php

namespace app\components;

/**
 * Description of ViewRenderer
 *
 * @author Pawan Kumar
 */
class ViewRenderer extends \yii\twig\ViewRenderer
{
    public function init()
    {
        parent::init();
        
        $this->twig->addGlobal('app', NULL);
        $this->twig->addExtension(new \Twig_Extensions_Extension_Text());
        $this->twig->addExtension(new \Jasny\Twig\PcreExtension());
        $this->twig->addExtension(new twig_extension\ContentExtension());
        
        if (  defined('YII_DEBUG') && YII_DEBUG ) {
            $this->twig->enableDebug();
            $this->twig->addExtension(new \Twig_Extension_Debug());
        }
    }
}
