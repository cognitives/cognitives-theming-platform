<?php

namespace app\components;

/**
 * Description of XMLReaderComponent
 *
 * @author Rohit Gupta
 */
class XMLReaderComponent extends \yii\base\Component
{
    public static function readXMLFile($filePath)
    {
        $simpleXMLElementObj = simplexml_load_file($filePath, null, LIBXML_NOCDATA);
        $jsonEncoded = json_encode($simpleXMLElementObj);
        $dataArr = json_decode($jsonEncoded, TRUE);
        
        return self::replaceNullValues($dataArr);
    }
    
    public static function replaceNullValues($arr)
    {
        foreach($arr as $key => $val) {
            
            if(is_array($val)) {
                $arr[$key] = self::replaceNullValues($val);
            }
            else {
                if($val == 'NULL') {
                    $arr[$key] = NULL;
                }
                else {
                    $arr[$key] = $val;
                }
            }
        }
        
        return $arr;
    }
}
