<?php

namespace app\components;

use Yii;

/**
 * Description of Network
 *
 * @author Rohit Gupta
 */
class NetworkComponent extends \yii\base\Component
{
    private $networkId = null;
    protected $data = null;
    protected $plan = null;
    protected $planSetting = null;
    protected $servicePacks = null;
    protected $channelModel;
    
    public function setupNetwork()
    {
        $this->data = \app\models\Network::getNetworkData();
        $this->channelModel = \app\models\Network::getChannelData();
        $this->data['domain'] = $_SERVER['SERVER_NAME'];
        
        $this->networkId = $this->data['id'];
    }
    
    public function getData()
    {
        if (!isset($this->data)) {
            $this->data = \app\models\Network::getNetworkData();
            
            if(!isset($this->channelModel) || empty($this->channelModel)) {
                $this->channelModel = \app\models\Network::getNetworkData();
            }
        }
        
        return $this->data;
    }
    
    public function getChannelModel()
    {
        if (!isset($this->channelModel)) {
            $this->getData();
        }
        
        return $this->channelModel;
    }
}
