<?php

namespace app\components;

class SDKComponent extends \yii\base\Component
{
    private $_Network = [];
    
    public function setNetworkProperties($key, $data)
    {
        $this->_Network[$key] = $data;
    }
    
    public function getNetworkProperties($key)
    {
        if (isset($this->_Network[$key])) {
            return $this->_Network[$key];
        }
        
        return FALSE;
    }
}
