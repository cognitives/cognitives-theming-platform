<?php

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Description of ApplicationComponent
 *
 * @author Pawan Kumar
 */
class ApplicationComponent extends Component
{
    public $user;
    public $userBlogs;
    public $blog = null; //this gets set in AppBootstrap
    public $network = null;
    public $blogData = null;
    public $networkDefaultDomain = null;
    public $channelPartner = null;
    
    public function setupApplication()
    {
        $networkId = (int)\Yii::$app->network->data['id'];
        
        //if userNetworkRole is not set then user got logged in without LoginForm model. SHOULDN'T ever happen.
        if (!\Yii::$app->user->isGuest && (!\Yii::$app->session->has('userNetworkRole') || empty(\Yii::$app->session->get('userNetworkRole')))) {
            \Yii::$app->user->logout();
        }
    }
    
    public function hasUserBlogOwnerRights()
    {
        return true;
    }
    
    public function hasUserNetworkAdminRights()
    {
        return true;
    }
    
    //this gets set in AppBootstrap
    public function setupNetwork()
    {
        if (!isset($this->network)) {
            Yii::$app->network->setupNetwork();
            $this->network = Yii::$app->network;
        }
    }
    
    //this gets set in AppBootstrap
    public function setupBlog($blogId = '')
    {
        $this->blog = \app\models\Blog::getBlogByGuidAndOptionalNetworkId($blogId);
    }
    
    public function registerBlogMetaTags()
    {
        if(!isset($this->blog)) {
            return FALSE;
        }
        
        $blog = $this->blog;
       
        $keywords = '';
        if (isset($blog['keywords'])) {
            $keywords = rtrim($blog['keywords'], ',');
        }
        else if (isset($blog['admin_keywords'])) {
            $keywords .= ',' . rtrim($blog['admin_keywords'], ',');
        }

        \Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => htmlspecialchars($blog['title'])], 'title');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $keywords], 'keywords');
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => htmlentities($blog['short_description'])], 'description');

        /**
         * Twitter Meta tags set
         */
        \Yii::$app->view->registerMetaTag(['name' => 'twitter:title', 'content' => htmlspecialchars($blog['title'])], 'twitter:title');
        \Yii::$app->view->registerMetaTag(['name' => 'twitter:description', 'content' => htmlentities($blog['short_description'])], 'twitter:description');
       
        /**
         * Facebook Meta Tags Setting
         */
        \Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => htmlspecialchars($blog['title'])], 'og:title');
        \Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => htmlspecialchars($blog['short_description'])], 'og:description');
        \Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => \yii\helpers\Url::current([], true)], 'og:url');
        \Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => $this->network->data['title']], 'og:site_name');
    }
}
