<?php
namespace app\components;

class AppBootstrap implements \yii\base\BootstrapInterface
{
    public function bootstrap($app) 
    {
        $appParams = $app->params;
        if(!isset($appParams['themeDir']) || empty($appParams['themeDir'])) {
            throw new \yii\web\NotFoundHttpException("Please add a 'themeDir' array element in config/params-local.php file & set it to your theme directory.");
        }
        if(!isset($appParams['dataFileName']) || empty($appParams['dataFileName'])) {
            throw new \yii\web\NotFoundHttpException("Please add a 'dataFileName' array element in config/params-local.php file & set it to your data json file that you earlier exported from platform.");
        }
        
        $themeDir = $appParams['themeDir'];
        $themePath = $appParams['themesDirPath'] . '/' . $themeDir;
        $layoutsPath = $themePath . '/layouts';
        
        if(!is_dir($themePath) || !is_dir($layoutsPath)) {
            throw new \yii\web\NotFoundHttpException("No theme found. Please clone a theme from Github or create your own in web/themes/{$themeDir} directory.");
        }
        
        //setting the base layout and theme
        $app->viewPath = $themePath;
        $app->layoutPath = $layoutsPath;
        
        $url = $app->request->absoluteUrl;
        $blogSubdomain = '';
        $blogId = '';
        $urlWithAtSign = false;

        //if there is @ sign in the URL
        $matches = [];
        if (preg_match('/^.+?\/@(.+)$/i', $url, $matches)) {
            $paramsArr = explode('/', $matches[1]);
            $blogSubdomain = $paramsArr[0];
            $urlWithAtSign = true;
        }

        //Set url rules
        $routePrefix = ($urlWithAtSign) ? "@$blogSubdomain" : "";
        $routePrefixWithSlash = ($urlWithAtSign) ? $routePrefix . "/" : "";
        $routes = [
            "$routePrefix" => 'home/index', 
            "{$routePrefixWithSlash}<slug:\w+[-\w+]+>" => "home/page",
        ];
        

        if ($urlWithAtSign) {
            $blogId = \app\models\Blog::getBlogByUrlPrefix($blogSubdomain);
            if(empty($blogId)) {
                throw new \Exception("This section or page cannot be found.");
            }
            $routes = [
                "$routePrefix" => 'home/index',
                "{$routePrefixWithSlash}<home:([\w\-./])+>" => "home/index" 
            ];
        }
        $app->getUrlManager()->addRules($routes, false);


        $app->application->setupBlog($blogId);
        
        //Setting Application Network Components based on network
        $app->application->setupNetwork();
    }
}
