<?php

namespace app\components\twig_extension;


/**
 * Description of ContentExtension
 *
 * @author Aman Dhaliwal
 */
class ContentExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('_resizeImages', [$this, 'modifyImageUrls']),
            new \Twig_SimpleFilter('usort', [$this, 'usortFilter'])
        ];
    }
    
    public function modifyImageUrls($content, $args)
    {
        $regex = '#(\'|\")(http|https):\/\/res.cloudinary.com\/(.*?)\/image\/upload\/(.*?)\.(.*?)(\'|\")#';
        return preg_replace_callback($regex, function($matches) use($args){
            
            if(count($matches) < 5) {
                return $matches[0];
            }
            $arr = explode('/', $matches[4]);
            if(count($arr) < 2) {
                return $matches[0];
            }
            $cloudinaryPublicId = $arr[1];
            if(empty($cloudinaryPublicId)) {
                return $matches[0];
            }
            
            $scheme = $matches[2];
            $cloud = $matches[3];
            $ext = $matches[5];
            $widthHeight = "";
            if(isset($args['width']) && $args['width'] > 0) {
                $widthHeight = "w_" . $args['width'] . ",";
            }
            if(isset($args['height']) && $args['height'] > 0) {
                $widthHeight .= "h_" . $args['height'] . ",";
            }
            
            $imageUrl = $scheme . "://res.cloudinary.com/" . $cloud . "/image/upload/c_limit,dpr_auto,f_auto,fl_lossy," . $widthHeight . "q_auto/" . $cloudinaryPublicId . "." . $ext;
            return $imageUrl;
        }, $content);
    }
    
    public function usortFilter($items)
    {
        $arr = [];
        foreach($items as $item) {
            if(!isset($item['additionalInfo']['position']) || empty($item['additionalInfo']['position'])) {
                $item['additionalInfo']['position'] = 1;
            }
            
            $arr[$item['additionalInfo']['position']] = $item;
        }
        
        ksort($arr);        
        return array_values($arr);
    }
}
