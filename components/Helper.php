<?php

namespace components;

use Yii;

/**
 * Description of Helper
 *
 * @author Aman Dhaliwal
 */
class Helper
{

    public static function htmlToText($html, $maxLen = -1)
    {
        $txtOnly = strip_tags($html);

        $txtOnly = trim(str_replace('&nbsp;', ' ', $txtOnly));
        $txtOnly = preg_replace('/\s+/', ' ', $txtOnly);

        $txtOnly = preg_replace('/((?<=[A-Za-z0-9])\.(?=[A-Za-z]{2})|(?<=[A-Za-z]{2})\.(?=[A-Za-z0-9]))/', '. ', $txtOnly);

        if ($maxLen > 0) {
            $txtOnly = self::shortStr($txtOnly, $maxLen, false, true);
        }

        return $txtOnly;
    }

    public static function shortStr($str, $len, $cut = false, $ellipsis = false)
    {
        if (strlen($str) <= $len) {
            return $str;
        }
        $string = ( $cut ? (($ellipsis) ? substr($str, 0, $len - 2) : substr($str, 0, $len) ) : substr($str, 0, strrpos(substr($str, 0, $len), ' ')) );
        if ($ellipsis) {
            $string .= "...";
        }
        return $string;
    }

    public static function getEstimatedReadingTime($string)
    {
        $words = str_word_count(strip_tags($string));
        $min = floor($words / 180);
        return ($min == 0) ? 1 : $min;
    }

    public static function renderReadingTime($time)
    {
        if ($time <= 59) {
            return $time . ' min read';
        }
        return round($time / 60) . 'hour read';
    }

    public static function outputJsonResponse($returnArr = [])
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $returnArr;
    }

}
