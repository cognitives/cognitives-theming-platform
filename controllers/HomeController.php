<?php

namespace app\controllers;

use Yii;
use app\controllers\AppController;

class HomeController extends AppController
{

    public function actionIndex()
    {
        $viewFile = 'index';
        $blog = \Yii::$app->application->blog;
        
        if (isset($blog['blogLayoutFileName']) && is_file(Yii::$app->getViewPath()."/". $blog['blogLayoutFileName'] . '.twig') ) {
            $viewFile = $blog['blogLayoutFileName'];
        } else {
            $viewFile = ($blog['is_default']) ? $viewFile : $blog['type'];
        }

        if ($viewFile == 'channel' && !is_file(Yii::$app->getViewPath()."/".$viewFile . '.twig')) {
            $viewFile = 'section'; 
        } 
        
        return $this->render($viewFile . '.twig');
    }

    public function actionLoadArticles()
    {
        $offset = (int) Yii::$app->request->post('offset');
        $limit = (int) Yii::$app->request->post('limit');

        $existingNonPinnedCount = (int) Yii::$app->request->post('existingNonPinnedCount');
        $dateFormat = Yii::$app->request->post('dateFormat', '');
        $articlesArr = \app\models\BlogFeed::getBlogFeed(['is_default' => true]);
        $articles = [];
        $pos = $offset + 1;

        $articlesArr = array_slice( $articlesArr, $offset, $limit );
        
        foreach ($articlesArr as $article) {
            $article['position'] = $pos;
            $articles[] = $article;
            $pos++;
        }

        return \components\Helper::outputJsonResponse([
                    'success' => 1,
                    'articles' => $articles,
                    'existingNonPinnedCount' => 20
        ]);
    }

    public function actionPage($slug)
    {
        if (empty($slug)) {
            throw new \yii\web\HttpException(500, "Page not found");
        }

        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'page page-' . $slug;

        $pageSdk = new \sdk\frontend\Page();
        $pageData = $pageSdk->getPageData($slug);

        if (!empty($pageData['meta_title'])) {
            \Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => htmlspecialchars($pageData['meta_title']), 'id' => 'title'], 'title');
        }

        if (!empty($pageData['meta_keyword'])) {
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $pageData['meta_keyword'], 'id' => 'keywords'], 'keywords');
        }

        if (!empty($pageData['meta_description'])) {
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $pageData['meta_description']], 'description');
        }

        /** System Exist static Page Then Load Static other wise content come through backend * */
        $filename = $slug . '.' . 'twig';
        $twigTemplate = file_exists(\Yii::$app->viewPath . '/' . $filename) ? $filename : 'page.' . 'twig';

        return $this->render($twigTemplate, [
                    'slug' => $slug,
                    'page' => $pageData
        ]);
    }

    public function actionContactUs()
    {
        return $this->redirect(\yii\helpers\Url::toRoute('page/contact-us'));
    }

    public function actionPinArticle()
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

    public function actionDeleteArticle()
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

    public function actionSwapArticle()
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

}
