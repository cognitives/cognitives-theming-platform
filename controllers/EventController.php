<?php

namespace app\controllers;

/**
 * Description of EventController
 *
 * @author Azam
 */
class EventController extends AppController
{
    public function actionIndex()
    {
        $networkGuid = \Yii::$app->network->data['guid'];

        $eventObj = new \sdk\frontend\Event;

        $events = $eventObj->getEvents(['networkGuid' => $networkGuid]);

        return $this->render('events-list.twig', [
            'events' => $events
        ]);
    }

}
