<?php

namespace app\controllers;

use Yii;

/**
 * Description of AuthController
 *
 * @author Aman Dhaliwal
 */
class AuthController extends AppController
{

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'offset' => 2,
                'testLimit' => 1,
            //'fontFile' => '@common/widgets/captcha/fonts/arialblackbold.ttf'
            ],
        ];
    }

    public function actionLogin($signup = 0)
    {
        if (\Yii::$app->request->isPost) {
            $session = Yii::$app->session;
            $session->set('login', '1');
            return $this->redirect(\yii\helpers\Url::toRoute('/'));
        }

        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'auth login';

        return $this->render('login.' . 'twig', [
                    'login' => (!$signup) ? TRUE : FALSE,
                    'signup' => ($signup) ? TRUE : FALSE,
        ]);
    }

    public function actionLoginModal()
    {
        $refererUrl = Yii::$app->request->get('ref');
        if ($refererUrl == '') {
            $refererUrl = 'http://' . \Yii::$app->request->serverName;
        }
        else if (strstr($refererUrl, 'http://') === false) {
            $refererUrl = 'http://' . \Yii::$app->request->serverName . Yii::$app->request->get('ref');
        }

        if ($refererUrl != '') {
            \Yii::$app->session->set('refererUrl', $refererUrl);
        }

        $isNetworkClosed = FALSE;

        return $this->renderPartial('partials/login-modal.' . 'twig', [
                    'isNetworkClosed' => $isNetworkClosed
        ]);
    }

    public function actionSignup()
    {
        return $this->render('login.' . 'twig');
    }

    public function actionThankYou()
    {
        return $this->render('thank-you.' . 'twig');
    }

    public function actionLogoff()
    {
        $session = Yii::$app->session;
        if ($session->has('login')) {
            $session->remove('login');
        }

        return $this->redirect(\yii\helpers\Url::toRoute('/'));
    }

    public function actionForgotPassword()
    {

        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'auth forgot-password';

        return $this->render('forgot-password.' . 'twig');
    }

    public function actionForgotThanks()
    {
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'auth forgot-password-thanks';

        return $this->render('forgot-thanks.' . 'twig');
    }

    public function actionResetPassword($token)
    {
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'auth reset-password';
        
        return $this->render('reset-password.' . 'twig');
    }

    public function actionResetThanks()
    {
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'auth reset-password-thanks';

        return $this->render('reset-thanks.' . Yii::$app->params['templateFileExtension']);
    }

    /**
     *  User Email Verification  action process here
     * @param string $token
     * @return void
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        $status = 1;
        return $this->redirect(['verify-email-thanks', 'status' => $status]);
    }

    /**
     * Email Verification Thankyou message
     * 
     * @param integer $status
     * @return array
     */
    public function actionVerifyEmailThanks($status)
    {
        return $this->render('verify-email-thanks.' . 'twig', [
                    'success' => $status
        ]);
    }

}
