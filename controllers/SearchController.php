<?php

namespace app\controllers;

/**
 * Description of SearchController
 *
 * @author Aman Dhaliwal
 */
class SearchController extends AppController
{
    public function actionIndex()
    {
        $searchTerm = filter_var(\Yii::$app->request->get('s'), FILTER_SANITIZE_STRING);
        
        return $this->render('search' . '.' . 'twig', 
                ['search' => $searchTerm]);
    }
    
    public function actionLoadArticles()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(500, \Yii::t('app', 'Invalid request'));
        }
        
        if(\Yii::$app->request->post('limit') === NULL || (int)\Yii::$app->request->post('limit') <= 0) {
            throw new \yii\web\HttpException(500, \Yii::t('app', 'Invalid request'));
        }
        
        $articlesArr = \sdk\frontend\Search::searchArticles([
            'offset' => (int)\Yii::$app->request->post('offset'),
            'limit' => (int)\Yii::$app->request->post('limit'),
            'search' => filter_var(\Yii::$app->request->post('search'), FILTER_SANITIZE_STRING)
        ]);

        
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'search';
        
        return \components\Helper::outputJsonResponse([
            'success' => 1,
            'articles' => $articlesArr
        ]);
    }    
}
