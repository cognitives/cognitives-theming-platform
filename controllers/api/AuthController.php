<?php

namespace app\controllers\api;

use Yii;
use app\controllers\api\ApiController;

/**
 * Description of AuthController
 *
 * @author Azam
 */
class AuthController extends ApiController
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    '*' => ['post']
                ],
            ],
        ];
    }

    public function actionSignup()
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

    public function actionLogin()
    {
        $session = Yii::$app->session;
        $session->set('login', '1');
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

    public function actionForgotPassword()
    {

        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

    public function actionResetPassword($token)
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

}
