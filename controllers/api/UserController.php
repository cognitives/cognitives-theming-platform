<?php

namespace app\controllers\api;

use Yii;
use app\controllers\api\ApiController;

/**
 * Description of UserController
 *
 * @author Azam
 */
class UserController extends ApiController
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    '*' => ['post']
                ],
            ],
        ];
    }

    public function actionEditProfile()
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

    public function actionFollow()
    {
        return \components\Helper::outputJsonResponse([
                    'success' => 1
        ]);
    }

}
