<?php

namespace app\controllers\api;

use Yii;
use app\controllers\api\ApiController;

/**
 * Description of SocialController
 *
 * @author Rohit Gupta
 */
class SocialController extends ApiController
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'get-social-post' => ['post']
                ],
            ],
        ];
    }

    public function actionGetSocialPost()
    {
        $socialFeedGuid = Yii::$app->request->post('guid');
        $blogGuid = Yii::$app->request->post('blog_guid');

        $socialFeedObj = new \sdk\frontend\SocialFeed;
        return $socialFeedObj->getSocialPost($socialFeedGuid, $blogGuid);
    }

}
