<?php

namespace app\controllers;

use Yii;
use app\controllers\AppController;

class ArticleController extends AppController
{
    public function actionIndex()
    {
        $articleId = (int) Yii::$app->request->get('id');
        
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'article article-' . $articleId;
        
        $eventObj = new \sdk\frontend\Event;
        $event = $eventObj->getEventDetails(['eventId' => $articleId]);
        
        if(count($event) > 0) {
            return $this->render('event.twig',[
                'event' => $event,
            ]);
        }

        $articleObj = new \sdk\frontend\Article;
        $articleModel = $articleObj->getArticle($articleId, []);

        $viewFile = $articleModel['type'] == \app\models\Article::TYPE_ARTICLE ? "article" : "event";

        if (isset($articleModel['articleLayoutFileName']) && is_file(Yii::$app->getViewPath()."/".$articleModel['articleLayoutFileName'] . '.twig')) {
            $viewFile = $articleModel['articleLayoutFileName']; 
        } 
        return $this->render($viewFile.'.twig',[
            'article' => $articleModel,
        ]);
    }
}
