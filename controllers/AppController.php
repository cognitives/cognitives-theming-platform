<?php

namespace app\controllers;

use Yii;

class AppController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        \Yii::$app->application->setupApplication();

        //relative
        $templatePath = '/' . Yii::$app->params['themesDirRelativeHttpPath'] . '/' . Yii::$app->params['themeDir'];
        Yii::setAlias('templatePath', $templatePath);

        //setting the controller view path to be same as viewPath of the application as all the files are in the same directory
        Yii::$app->controller->viewPath = Yii::$app->viewPath;

        Yii::$app->view->params['bodyClass'] = '';

        return parent::beforeAction($action);
    }
    
    public function render($view, $params = array())
    {
        if(!file_exists(Yii::$app->viewPath . '/' . $view)) {
            throw new \Exception('View File does not exist!!');
        }
        
        $this->initGlobalJs();
        
        return parent::render($view, $params);
    }

    public function initGlobalJs()
    {
        $isUserLogin = 1;
        $userHasBlogAccess = 1;

        $jsContstant = [
            'baseHttpPath' => '',
            'isUserLoggedIn' => $isUserLogin,
            'userHasBlogAccess' => $userHasBlogAccess,
            'articleOffset' => 20,
            'cloudName' => '',
            'uploadPreset' => '',
            'filepickerKey' => 'AkgotsVBS1Kfe4DTluXMUz',
            'appHostName' => "http://" . \Yii::$app->request->hostName,
			'uploadImageFileFormat' => \Yii::$app->params['image.format'],
            'uploadImageMaxFilesize' => \Yii::$app->params['image.filesize'],
            'templatePath' => Yii::getAlias('@templatePath')
        ];

        $view = Yii::$app->view;

        Yii::$app->view->registerJs("var _appJsConfig = " . json_encode($jsContstant), $view::POS_HEAD);
    }

}
