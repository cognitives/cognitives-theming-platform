<?php

namespace app\controllers;

use Yii;
use app\controllers\AppController;

class ProfileController extends AppController
{

    public function actionUserProfile($name)
    {
        $username = filter_var($name, FILTER_SANITIZE_STRING);
        if (empty($username)) {
            throw new \yii\web\HttpException(500, 'Invalid request.');
        }

        $userModelArr = $this->getUserModel($username);

        $userObj = new \sdk\frontend\User;
        $blogObj = new \sdk\frontend\Blog;

        /**
         * user blog as owner
         */
        $ownerBlogs = $blogObj->getOwnerBlogs($userModelArr['guid']);


        /**
         * user recent articles
         */
        $myArticles = $userObj->getUserArticles($userModelArr['guid'], 0, 6);



        //User Followed By
        $myFollowers = $userObj->getUserFollowers($userModelArr['guid']);



        //User Followings
        $myFollowings = $userObj->getUserFollowings($userModelArr['guid']);



        //User Following Blogs:: Normal Blog
        $myBlogs = $userObj->getBlogsFollowedByUser($userModelArr['guid']);


        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'user profile profile-' . $username;

        return $this->render('user-profile.twig', [
                    'userOwnerBlogs' => $ownerBlogs,
                    'myArticles' => $myArticles,
                    'myFollowers' => $myFollowers,
                    'myFollowings' => $myFollowings,
                    'myBlogs' => $myBlogs,
                    'userDetails' => $userModelArr,
        ]);
    }

    public function actionUserPosts($name)
    {
        $username = filter_var($name, FILTER_SANITIZE_STRING);
        if (empty($username)) {
            throw new \yii\web\HttpException(500, 'Invalid request.');
        }

        $offset = 0;
        $limit = \app\models\Article::ARTICLE_LIMIT;
        if (Yii::$app->request->isAjax) {
            $newOffset = (int) \Yii::$app->request->post('offset');
            if ($newOffset > 0) {
                $offset = $newOffset;
            }
        }

        $userModelArr = $this->getUserModel($username);

        $userObj = new \sdk\frontend\User;
        $userArticles = $userObj->getUserArticles($userModelArr['guid'], $offset, $limit);

        $articleObj = new \sdk\frontend\Article;
        $totalArticles = 10;

        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'user user-articles user-articles-' . $username;

        return $this->render('user-posts.twig', [
                    'user' => $userModelArr,
                    'userArticles' => $userArticles,
                    'totalArticles' => $totalArticles,
                    'limit' => $limit
        ]);
    }

    protected function getUserModel($username)
    {
        $userModel = (new \sdk\frontend\User)->getUser(['username' => $username]);

        return $userModel;
    }

}
