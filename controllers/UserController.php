<?php

namespace app\controllers;

use Yii;
use app\controllers\AppController;

class UserController extends AppController
{
    public function actionEditProfile()
    {       
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'user edit-profile';
         if (Yii::$app->request->isPost) {
             \Yii::$app->session->setFlash('success', 'Profile updated successfully.');
         }
        return $this->render('edit-profile.twig');
    }

    public function actionMyNews()
    {
                
        //setting body classes
        \Yii::$app->view->params['bodyClass'] .= 'user my-news';
        
        return $this->render('my-news.twig');
    }
    
    public function actionFollowArticle()
    {
        return \components\Helper::outputJsonResponse(['success' => 1]);
    }
    
    public function actionFollowUser()
    {
        return \components\Helper::outputJsonResponse(['success' => 1]);
    }
    
    /**
     * Follow / Un-follow a club or blog
     * @return boolean
     */
    public function actionFollowBlog()
    {
        return \components\Helper::outputJsonResponse(['success' => 1]);
    }
    
    public function actionLoadArticles()
    {
        $offset = 0;
        $limit = 20;
        if(\Yii::$app->request->isAjax) {
            $limit = (int) \Yii::$app->request->post('limit');
            $page = (int) \Yii::$app->request->post('page');
            $offset = ($page - 1) * $limit;
        }
        
        $params['limit'] = $limit;
        $params['offset'] = $offset;
              
        $userPosts = \app\models\BlogFeed::getBlogFeed();
        if(\Yii::$app->request->isAjax) {
            return \components\Helper::outputJsonResponse(['success' => 1, 'articles' => $userPosts]);
        }
        return $userPosts;
    }

}
