<?php
return [
    'dataFilesPath' => dirname(__FILE__) . '/../data',
    'themesDirPath' => dirname(__FILE__) . '/../web/themes',
    'themesDirRelativeHttpPath' => 'themes',
    'applicationEnv' => isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : 'PROD',
    
    'themeDir' => '',
    'dataFileName' => '',
    
    'staticHttpPath' => 'http://' . $_SERVER['SERVER_NAME'] . '/static',
	
	//filestack
    'filestack.api.key' => 'AkgotsVBS1Kfe4DTluXMUz',
	
    'image.format' => '.jpg,.jpeg,.png,.gif,.bmp',
    
    'image.filesize' => 20*1024*1024, // 20 MB
    
    'video.maxfilesize' => 100*1024*1024,  //100 MB
];
