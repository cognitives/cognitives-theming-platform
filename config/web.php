<?php
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$config = [
    'id' => 'cog-theming',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\components\AppBootstrap'],
    'defaultRoute' => 'home/index',
    'layout' => 'main.twig',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'd41d8cd98f00b204e9800998ecf8427e',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                "" => "home/index",
                //"@myblog" => "home/index",
                //"@mysection" => "home/index",
                "<year:\d{4}>/<month:\d{2}>/<day:\d{2}>/<id:\d+>" => "article/index", //date_id_title (not title)
                "<year:\d{4}>/<month:\d{2}>/<day:\d{2}>/<id:\d+>/<slug:\w+[-\w+]+>" => 'article/index', //date_id_title
                "<id:\d+>" => 'article/index', //id_title but no title
                "<id:\d+>/<slug:\w+[-\w+]+>" => 'article/index', //id_title
                "home/load-articles" => 'home/load-articles', 
                "search/load-articles" => 'search/load-articles',         
                "search" => 'search/index',
                "home/pin-article" => 'home/pin-article',
                "home/delete-article" => 'home/delete-article',
                "home/swap-article" => 'home/swap-article',
                "user/my-news" => "user/my-news",
                "user/load-articles" => "user/load-articles",
                "user/follow-blog" => "user/follow-blog",
                "user/follow-user" => "user/follow-user",
                "user/follow-article" => "user/follow-article",
                "user/edit-profile" => "user/edit-profile",
                "feed/rss" => "feed/rss",
                "feed/sitemap" => "feed/sitemap",
                "feed/google-sitemap" => "feed/google-sitemap",
                "auth/captcha" => "auth/captcha",
                "article/disqus-comment" => "article/disqus-comment",
                "auth/login-modal" => "auth/login-modal",
                "auth/login" => "auth/login",
                "auth/forgot-password" => "auth/forgot-password",
                "auth/forgot-thanks" => "auth/forgot-thanks",
                "auth/reset-password" => "auth/reset-password",
                "auth/reset-thanks" => "auth/reset-thanks",
                "auth/thank-you" => "auth/thank-you",
                "auth/verify-email-thanks" => "auth/verify-email-thanks",
                "profile/<name:\w+>" => "profile/user-profile",
                "profile/<name:\w+>/posts" => "profile/user-posts",
                "page/<slug:\w+[-\w+]+>" => "home/page",
            ],
        ],
        'sdk' => [
            'class' => 'app\components\SDKComponent'
        ],
        'application' => [
            'class' => 'app\components\ApplicationComponent'
        ],
        'network' => [
            'class' => 'app\components\NetworkComponent',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'enableSession' => true,
            'loginUrl' => ['auth/login'],
        ],
        'view' => [
            'renderers' => [
                'twig' => [
                    'class' => 'app\components\ViewRenderer',
                    'cachePath' => false,
                    'options' => [
                        'auto_reload' => true,
                        'debug' => true
                    ],
                    'globals' => [
                        'Html'           => new yii\helpers\Html,
                        'Helper'         => new components\Helper,
                        '_SocialFeed'    => new sdk\frontend\SocialFeed,
                        '_Network'       => new sdk\frontend\Network,
                        '_Article'       => new sdk\frontend\Article,
                        '_Media'         => new sdk\frontend\Media,
                        '_Blog'          => new sdk\frontend\Blog,
                        '_Notification'  => new sdk\frontend\Notification,
                        '_User'          => new sdk\frontend\User,
                        '_AppHelper'     => new sdk\frontend\AppHelper,
                        '_Search'        => new sdk\frontend\Search,
                        '_AppForm'       => new sdk\frontend\AppForm,
                        '_Page'          => new sdk\frontend\Page,
                        '_Channel'       => new sdk\frontend\Channel,
                        '_CallToAction'  => new \sdk\frontend\CallToAction,
                        '_Event'         => new \sdk\frontend\Event,
                        '_MetaInfo'  => new \sdk\frontend\MetaInfo,
                        '_ContentBlock'  => new \sdk\frontend\ContentBlock

                    ]
                ]
            ],
            
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
            ],
        ],
    ],
    'params' => $params,
    
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
}

Yii::setAlias('@sdk', dirname(dirname(__DIR__)) . '/sdk');

return $config;
