<?php

namespace sdk\frontend;

use Yii;


class SocialFeed extends \sdk\base\SocialFeed
{

    public function getSocialPost($guid, $blogGuid)
    {
        $type = 'video';

        return \components\Helper::outputJsonResponse(self::getSocialFeedArray($type, $guid));
    }

    public static function getSocialFeedArray($type, $guid = NULL)
    {
        return \app\models\SocialFeed::getSocialFeed([
            'type' => $type,
            'guid' => $guid
        ]);
    }

    public function getSocialArticleSourceIsTwitter()
    {
        return \app\models\SocialFeed::SOURCE_TWITTER;
    }

    public function getSocialArticleSourceIsFacebook()
    {
        return \app\models\SocialFeed::SOURCE_FACEBOOK;
    }

    public function getSocialArticleSourceIsInstagram()
    {
        return \app\models\SocialFeed::SOURCE_INSTAGRAM;
    }

    public function getSocialArticleSourceIsYoutube()
    {
        return \app\models\SocialFeed::SOURCE_YOUTUBE;
    }

    public function getSocialArticleSourceIsVimeo()
    {
        return \app\models\SocialFeed::SOURCE_VIMEO;
    }

    public function getSocialArticleSourceIsGoogle()
    {
        return \app\models\SocialFeed::SOURCE_GOOGLE;
    }

    public function getSocialArticleSourceIsPinterest()
    {
        return \app\models\SocialFeed::SOURCE_PINTEREST;
    }

}
