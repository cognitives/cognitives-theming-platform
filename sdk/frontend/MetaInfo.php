<?php

namespace sdk\frontend;
use Yii;

/**
 * Description of MetaInfo
 *
 * @author Azam
 */
class MetaInfo extends \sdk\base\MetaInfo
{    
    const ARTICLE_TYPE = 'article';
    const BLOG_TYPE = 'blog';
    const USER_TYPE = 'user';
    const MEDIA_TYPE = 'media';

    public function getMetaData($entity, $entity_guid)
    {
        if(!is_numeric($entity_guid)) {
            switch ($entity) {
                case static::ARTICLE_TYPE:
                    $res = \app\models\Article::getArticle($entity_guid);
                    break;
                case static::BLOG_TYPE:
                    $res = \app\models\Blog::getBlog($entity_guid);
                    break;
                case static::USER_TYPE:
                    $res = [];
                    break;
                case static::MEDIA_TYPE:
                    $res = \app\models\Media::getMediaByGuid($entity_guid);
                    break;
                default:
                    return [];
            }
            
            if(empty($res)) {
                return[];
            }
            return (isset($res['additionalInfo']) && !empty($res['additionalInfo'])) ? $res['additionalInfo'] : [];

        }

    }
    
}
