<?php

namespace sdk\frontend;

use Yii;

/**
 * Description of Search
 *
 * @author Aman Dhaliwal
 */
class Search extends \sdk\base\Search
{
    /**
     * Search articles for a given search term
     * @param array $params - search=search-term, limit-integer, offset-integer
     * @return array - array of articles
     */
    public static function searchArticles($params)
    {
        $searchTerm = "";
        if (isset($params['search']) && !empty($params['search'])) {
            $searchTerm = filter_var($params['search'], FILTER_SANITIZE_STRING);
        }

        $limit = \app\models\Article::ARTICLE_LIMIT;
        $offset = 0;

        if (isset($params['limit']) && (int) $params['limit'] > 0) {
            $limit = (int) $params['limit'];
        }
        if (isset($params['offset']) && (int) $params['offset'] >= 0) {
            $offset = (int) $params['offset'];
        }
        $articles = \app\models\Search::searchArticles();
        if(isset($params['countOnly']) && !empty($params['countOnly']) ){
            return count($articles);
        }  
        if(isset($params['getUsers']) && !empty($params['getUsers']) ){
            $userArr = [];
            $users = \app\models\User::getUsers();
            $count=0;
            foreach($users as $user){
                $user['userId']= $user['id'];
                $user['name']= "$user[firstname] $user[lastname]";
                $user['media']=$user['profileMedia'];
                $userArr[$user['id']]=$user;
                if($count>3) break;
                $count++;
            }

            return $userArr;
        }        
        return $articles;
    }
}
