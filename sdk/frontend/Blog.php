<?php

namespace sdk\frontend;

use Yii;

class Blog extends \sdk\base\Blog
{

    public $blogOwner = null;
    public $blogsUrl = [];
    public $blogSettings;

    public function getTitle()
    {
        return \Yii::$app->application->blog['title'];
    }
    
    public function getBlogTitle()
    {
        return $this->getTitle();
    }
    
    public function getAll($params = [])
    {
        return \app\models\Blog::getBlogList($params);
    }
    
    public function get($params = [])
    {
        if (!empty($params['url'])) {
            $blogGuid = \app\models\Blog::getBlogByUrlPrefix($params['url']);
            if(!empty($blogGuid)) {
                $params['blogGuid'] = $blogGuid;
            }
        }
        $blog=[];

        if (!empty($params['blogGuid'])) {
            $blog = \app\models\Blog::getBlogByGuidAndOptionalNetworkId($params['blogGuid']);
        }else if(isset($params['title']) && !empty($params['title'])){
            $blog = \app\models\Blog::getBlogByTitle($params['title']);
        }
        if(empty($blog)){
            $blog = \app\models\Blog::getBlogByGuidAndOptionalNetworkId();
        }
        $data = \app\models\Blog::getBlog($blog['guid']);
        $data['userHasBlogAccess'] = 1;
        $data['isBlogOwner'] = 1;
        $data['blogHeaderMenu'] = $this->_defaultBlogMenu(\app\models\Menu::MENU_TYPE_HEADER);
        $data['blogFooterMenu'] = $this->_defaultBlogMenu(\app\models\Menu::MENU_TYPE_FOOTER);

        return $data;
    }

    public function getBlog($params = [])
    {
        $params['url'] = Yii::$app->request->url;
        if(isset($params['guid'])) {
            $params['blogGuid'] = $params['guid'];
        }

        return $this->get($params);
    }

    public function getBlogFeed($params = [])
    {
        $articles = \app\models\BlogFeed::getBlogFeed($params);
        if(isset($params['blogGuid']) && $params['blogGuid']){
            $blog = \app\models\Blog::getBlog($params['blogGuid']);
            $params['url']=$blog['url'];
            $articles = \app\models\BlogFeed::getBlogFeedByUrl($params);
        }
        $blogPostsNonPinned = count($articles);

        $retArr = [
            'articles' => $articles,
            'existingNonPinnedCount' => $blogPostsNonPinned
        ];

        return $retArr;
    }

    protected function convertTimezone($timestamp)
    {
        try {
            $network = \Yii::$app->network->getData();
            if (isset($network) && !empty($network['timezone'])) {
                $toTimezone = $network['timezone'];
                $fromTimezone = 'UTC';
                $date = new \DateTime('@' . strtotime($timestamp), new \DateTimeZone($fromTimezone));
                $date->setTimezone(new \DateTimeZone($toTimezone));
                $timestamp = strtotime($date->format('M d Y'));
            }
            return $timestamp;
        }
        catch (\Exception $e) {
            return $timestamp;
        }
    }
    
    public function getBlogUrl($params = [])
    {
        if (!empty($params['id'])) {
            $blog = \app\models\Blog::getBlogByGuidAndOptionalNetworkId($params['id']);
            if (isset($blog['url'])) {
                return $blog['url'];
            }
        }

        return 'http://' . Yii::getAlias('@homeUrl');
    }

    public function getOwnerBlogs($userGuid)
    {
        if (empty($userGuid)) {
            return FALSE;
        }

        $blogs = \app\models\Blog::getOwnerBlogs();

        return $blogs;
    }

    public function getHomeBlogSettings()
    {
        $pageData = \app\models\Blog::getHomeBlogSettings();
        return $pageData;
    }

    public function getBlogList($type = NULL, $params = [])
    {
        $params['type'] = $type;
        return $this->getAll($params);
    }

    public function getAuthors($params = [])
    {
        $authors = (new User)->getAll();

        return $authors;
    }
    
    protected function _defaultBlogMenu($menuType)
    {
        return \app\models\Menu::getMenu();
    }

    public function getFeed($params = [])
    {
        return $this->getBlogFeed($params);
    }

    public function getSections()
    {
        return $this->getAll(['type' => \app\models\Blog::TYPE_CHANNEL]);     
    }

    public function getUrl($params = [])
    {
        return $this->getBlogUrl($params);
    }

    public function getUserBlogs($params = [])
    {
        if (isset($params['userGuid']) || !empty($params['userGuid'])) {
            $userBlogs = $this->getAll(['userGuid' => $params['userGuid']]);
            return $userBlogs;
        }
        return null;
    }

    public function getFeedUsers($params = [])
    {
        if(!isset($params['blogId'])) {
            return false;
        }
        
        $users = \app\models\User::getUsers(); 
        if(!isset($params['limit'])){
            $params['limit'] = count($users);
        }
        return array_splice($users,0,$params['limit']);
    } 

    public function getMetaAttributes()
    {   // 
        return [];
    }

    public function getPopularBlogsByViews($params =[]){
        return $this->getAll();
    }

    public function getLinkedCTA($params = [])
    {
        $blog = $this->get($params);
        if (empty($blog)) {
            return [];
        }
        return (new CallToAction())->getAll($params);
    }

    public function getLinkedContentBlock($params = [])
    {
        $blog = $this->get($params);
        if (empty($blog)) {
            return [];
        }
        return (new ContentBlock())->getAll($params);
    }    
}
