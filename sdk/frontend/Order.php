<?php

namespace sdk\frontend;

use Yii;
/**
 * Description of Order
 *
 * @author Ashish
 */
class Order extends \sdk\base\Order
{

    public function getAll($params = [])
    {
        return \app\models\Order::getOrderData();     
    }

}
