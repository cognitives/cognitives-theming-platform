<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace sdk\frontend;

/**
 * Description of Channel
 *
 * @author Pawan Kumar 
 */
class Channel extends \sdk\base\Channel
{
    public function getChannel()
    {
        return \app\models\Blog::getBlogList(['type' => 'section']);
    }

}
