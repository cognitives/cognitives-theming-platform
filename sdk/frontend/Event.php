<?php

namespace sdk\frontend;

/**
 * Description of Event
 *
 * @author Azam
 */
class Event 
{    
    /**
     * Get all events 
     * 
     * @param array $params
     * @return array
     */
    public function getEvents($params = [])
    {        
        $events = \app\models\Event::getAllEvents();
        
        return $events;        
    }
    
    /**
     * Get single event details
     * 
     * @param array $params
     * @return array
     */
    public function getEventDetails($params = [])
    {
        if(!isset($params['eventId'])) {
            return false;
        }
        
        $event = \app\models\Event::getEventDetails($params);
        
        return $event;
    }
    
    public function getRecentEvents($params = [])
    {
        $events = $this->getEvents();
        $data =[];
        if(!empty($events)){
            foreach($events as $event){
                $urlParse = [];
                if(!empty($event['url'])){
                    $urlParse = parse_url($event['url']);
                    $event['url'] = $urlParse['scheme']."://".$_SERVER['HTTP_HOST'].$urlParse['path'];
                   
                }
                $data[]=$event;
            }
            return $data;
        }
        return [];
    }
    
    public function getPreviousEvents($params = [])
    {
        return $this->getRecentEvents();
    }
}
