<?php

namespace sdk\frontend;

class Media extends \sdk\base\Media
{

    /**
     * @param array $mediaArr = [
     *                  'id' => 'clodinary_public_id',
     *                  'cloudName' => 'cloudinary_cloud_name',
     *                  'path' => 'cdn_path'
     *             ]
     * @param int $width
     * @param int $height
     * @param array $options
     */
    public static function getMediaUrl($mediaArr, $width = 0, $height = 0, $options = [])
    {
        $media = [];
        $media['cloudinary_public_id'] = (isset($mediaArr['id']) && !empty($mediaArr['id'])) ? $mediaArr['id'] : '';
        $media['cloudinary_cloud_name'] = (isset($mediaArr['cloudName']) && !empty($mediaArr['cloudName'])) ? $mediaArr['cloudName'] : '';
        $media['cdn_path'] = (isset($mediaArr['path']) && !empty($mediaArr['path'])) ? $mediaArr['path'] : '';

        $newWidth = (isset($width) && (int) $width > 0) ? $width : 0;
        $newHeight = (isset($height) && (int) $height > 0) ? $height : 0;

        $type = NULL;
        if (isset($options['type']) && !empty($options['type'])) {
            $type = $options['type'];
            unset($options['type']);
        }

        return \app\models\Media::getMediaUrl([
                    'media' => $media,
                    'width' => $newWidth,
                    'height' => $newHeight,
                    'type' => $type,
                    'cloudinary' => (isset($options) && !empty($options)) ? \yii\helpers\ArrayHelper::merge(['crop' => 'limit'], $options) : $options
        ]);
    }

    public static function getMediaVideoUrl($mediaArr, $width = 0, $height = 0, $options = [])
    {
        $media = [];
        $media['cloudinary_public_id'] = (isset($mediaArr['videoId']) && !empty($mediaArr['videoId'])) ? $mediaArr['videoId'] : '';
        $media['cloudinary_cloud_name'] = (isset($mediaArr['cloudName']) && !empty($mediaArr['cloudName'])) ? $mediaArr['cloudName'] : '';
        $media['cdn_path'] = 'http://res.cloudinary.com/cognitives/video/upload/c_limit,dpr_auto/ksjgabb06pcyedxgc6is.mp4';

        $newWidth = (isset($width) && (int) $width > 0) ? $width : 0;
        $newHeight = (isset($height) && (int) $height > 0) ? $height : 0;

        return \app\models\Media::getMediaUrl([
                    'media' => $media,
                    'width' => $newWidth,
                    'height' => $newHeight,
                    'cloudinary' => (isset($options) && !empty($options)) ? \yii\helpers\ArrayHelper::merge(['crop' => 'limit', 'resource_type' => 'video', 'format' => 'mp4'], $options) : $options
        ]);
    }
    public function get($params = [])
    {
        $medias = \app\models\Media::get();
        $mediaArr = [];
        if(!empty($medias)){
            if(!empty($params['keyword'])) {
                $i=1;
                foreach($medias as $media ){
                    if($i<5){
                        $mediaArr[] = $media;
                    }
                }
            }
            if (isset($params['id']) && !empty($params['id'])) {
                foreach($medias as $media ){                    
                    if($media['id']==$params['id']){
                        $mediaArr = $media;
                    }
                }
            }else{
                $params['returnAll'] = true;
            }
            if (empty($params['returnAll'])) {
                $mediaArr = [$mediaArr];
            }
            return ( !empty( $params['returnAll'])  ) ? $mediaArr : $mediaArr[0];
        }
        return $mediaArr;
    }

    public function getAssetCollections($params = [])
    {
        return [];
    }


}
