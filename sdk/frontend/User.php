<?php

namespace sdk\frontend;

use Yii;

class User extends \sdk\base\User
{

    public function isGuest()
    {
        return (Yii::$app->session->has('login')) ? FALSE : TRUE;
    }

    public function getId()
    {
        return (Yii::$app->session->has('login')) ? 1 : FALSE;
    }
    
    public function getAll($params = [])
    {
        return \app\models\User::getUsers();
    }
    
    public function get($params = [])
    {
        $userData = \app\models\User::getUsers();
        
        foreach($userData as $user) {
            if(isset($params['username']) && !empty($params['username']) && $params['username'] == $user['username']) {
                return $user;
            }
        }

        return isset($userData[0]) ? $userData[0] : [];
    }

    /**
     * Get logged-in user's data
     * @return boolean|array - user data
     */
    public function getUser($params = [])
    {
        return $this->get($params);        
    }
    
    /**
     * Get articles created by a user
     * @param string $userGuid
     * @param integer $offset
     * @param integer $limit
     * @return array - array of articles
     * @throws \yii\base\InvalidParamException
     */
    public function getUserArticles($userGuid, $offset = 0, $limit = 10)
    {
        return $this->getArticles($userGuid, $offset, $limit);
    }
    
    public function getArticles($userGuid, $offset = 0, $limit = 10)
    {
        $userArticles = \app\models\BlogFeed::getBlogFeed(['is_default' => true]);

        $articles = [];
        foreach ($userArticles as $article) {
            if(isset($article['socialId']) && $article['socialId'] > 0) {
                continue;
            }

            $article['createdBy']['username'] = 'username';
            $articles[] = $article;          
        }

        return $articles;        
    }

    /**
     * Get articles from user feed - created by user or articles from blogs followed by user
     * @param array $params - offset=integer, limit-integer
     * @return array - array of articles
     */
    public function getUserFeed($params = [])
    {
        return $this->getFeed($params);
    }
    
    public function getFeed($params = [])
    {
        $feeds = \app\models\BlogFeed::getBlogFeed(['is_default' => true]);

        return $feeds;
    }

    /**
     * Get users which are following a particular user
     * @param string $userGuid
     * @return array - array of users
     */
    public function getUserFollowers($userGuid)
    {
        return $this->getFollowers($userGuid);
    }
    
    public function getFollowers($userGuid)
    {
        $userFollowerArr = \app\models\User::getUserFollowers();

        return $userFollowerArr;
    }

    /**
     * Get an array of users followed by a particular user
     * @param string  $userGuid
     * @return array - array of users
     */
    public function getUserFollowings($userGuid)
    {
        return $this->getFollowings($userGuid);
    }
    
    public function getFollowings($userGuid)
    {
        $userFollowingsArr = \app\models\User::getUserFollowing();

        return $userFollowingsArr;
    }

    /**
     * 
     * @param string $type - entoty type (blog, user, article, writer)
     * @param integer $id - id of an entity of above type
     * @return boolean
     */
    public function isUserFollowing($type, $id)
    {
        return false;
    }

    /**
     * Get an array of blogs followed by a particular user
     * @param string $userGuid
     * @return array
     */
    public function getBlogsFollowedByUser($userGuid)
    {
        $followingBlogList = \app\models\User::getBlogsFollowedByUser();

        return $followingBlogList;
    }

    public function getProfileCompletionPercentage()
    {
        $profileCompletePercent = 50;

        $user = $this->getUser();

        //Check user has uploaded profile image or not
        if (count($user['profileMedia']) > 0 && !empty($user['profileMedia']['id'])) {
            $profileCompletePercent += 30;
        }

        if (isset($user['bio']) && !empty($user['bio'])) {
            $profileCompletePercent += 20;
        }

        return $profileCompletePercent;
    }
    
    public function getNetworkUsers()
    {
        return $this->getAll();
    }

    public function getPopularUsersByViews($params = [])
    {

        $popularUserViews = \app\models\User::getPopularUsersByViews();
        return $popularUserViews;

    }
    public function getPopularUsersByFollowers($params = [])
    {
        $popularUserFollowers = \app\models\User::getPopularUsersByFollowers();
        return $popularUserFollowers;

    }
    
    public function getPopularUsersByPosts($params = [])
    {
        $popularUserPost = \app\models\User::getPopularUsersByPosts();
        return $popularUserPost;

    }
    public function MailchimpSubscription(){
        return false;
    }

    public function getMetaAttributes()
    {
        return [];
    }
    public function isFollowing($type, $id)
    {
        if ((int) $id <= 0 || empty($type)) {
            return FALSE;
        }
        $userFollowing = \app\models\User::getUserFollowing();
        if(!empty($userFollowing)){
            foreach($userFollowing as $user){
                if($user['id']==$id){
                    return true;
                }
            }
        }
        return false;
    
    }
    public function hasContentBlockUpdatePermissions()
    {
        return false;
    }
    public function hasCtaUpdatePermissions()
    {
        return false;
    }
    public function isAdminUser()
    {
        return false;
    }
}
