<?php

namespace sdk\frontend;

/**
 * Description of ContentBlock
 *
 */
class ContentBlock extends \sdk\base\ContentBlock
{    

    public function getAll($params = [])
    {
        return \app\models\ContentBlock::getAll();
    }
    
    public function get($params = [])
    {
        $contentBlock = \app\models\ContentBlock::getAll();        
        $data=[];
        if(!empty($contentBlock)){
            if(isset($params['keyword']) &&!empty($params['keyword'])){
                foreach($contentBlock as $block ){
                    $keywords = explode(",",$block['keywords']);
                    if(in_array($params['keyword'],$keywords)){
                        $data[] =$block; 
                    }
                }
                return $data;
    
            }
            return \app\models\ContentBlock::getAll();
        }
        
        return [];
    }

    public function getKeywordBasedContentBlock($keywords)
    {
        $params['keyword'] = $keywords;
        return $this->get($params);
    }
    
    public function getByArticleId($articleId, $includeMetaInfo = false)
    {
        return \app\models\ContentBlock::getAll();
    }
   
    
}
