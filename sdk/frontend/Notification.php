<?php

namespace sdk\frontend;

class Notification
{

    public static function getNotifications()
    {
        return \app\models\Notification::getNotifications();
    }

}
