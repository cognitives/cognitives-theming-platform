<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace sdk\frontend;

use Yii;

/**
 * Description of Page
 *
 * @author Aman Dhaliwal
 */
class Page extends \sdk\base\Page
{
    /**
     * Get class name for body tag
     * @return string
     */
    public function getBodyClass()
    {
        return (isset(Yii::$app->view->params['bodyClass'])) ? Yii::$app->view->params['bodyClass'] : '';
    }
    
    /**
     * 
     * @param array $params - guid (page-guid)
     * @return string - Page url
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getPageUrl($params = [])
    {
        if(empty($params['guid'])) {
            throw new \yii\web\HttpException(500, "Page GUID not found");
        }

        // $page = \app\models\Page::getPageByGuid($params['guid']); 
        // return isset($page['slug']) ? '/page/' . $page['slug'] : '';
        $page = \app\models\Page::getPageDataByParams($params);

        if (!isset($page) || empty($page)) {
            throw new \yii\web\HttpException(500, "Page not found");
        }

        $pageSlug = !empty($page['slug']) ? $page['slug'] : $page['id'];

        if (isset($params['urlFormat']) && $params['urlFormat'] == 'relative') {
            return '/' . $pageSlug;
        }
        return  '/' . $pageSlug;
    }

    /**
     * Get page attributes based on its slug
     * @param string $slug
     * @return array - page data
     * @throws \yii\web\HttpException
     */
    public function getPageData($slug,$throwExceptionIfNotFound=true)
    {
        if(empty($slug)) {
            throw new \yii\web\HttpException(500, "Page not found");
        }
        
        $pageData = \app\models\Page::getPageData($slug);     
        
        return $pageData;
    }

    public function getAll($params = [])
    {
        $params['status'] = \app\models\Page::STATUS_ACTIVE_YES;
        $pages = \app\models\Page::getPageDataByParams($params);
        return $pages;
    }

    public function get($params = [],$throwExceptionIfNotFound=true)
    {
        return $this-> getAll($params);
    }

     public function getLayoutName()
    {
        return isset(Yii::$app->view->params['layout']) ? Yii::$app->view->params['layout'] : 'default-layout';
    }

    public function getLayoutTitle()
    {
        return isset(Yii::$app->view->params['layoutTitle']) ? Yii::$app->view->params['layoutTitle'] : 'default-layout';
    }

    public function setHttpRedirects($params = [], $passPath = false)
    {
        $url = $_SERVER['REQUEST_URI'];
        foreach ($params as $key => $value) {
            if (preg_match($key, $url) == 1 ) {
                if ($passPath === true) {$value = $value.$_SERVER['REQUEST_URI'];}
                \Yii::$app->getResponse()->redirect($value)->send();
            }
                
        }
    }
    
}
