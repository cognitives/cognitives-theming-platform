<?php

namespace sdk\frontend;

/**
 * Description of CallToAction
 *
 * @author Insphere
 */
class CallToAction
{
    /*
     * Get List of CTAs
     * 
     * @param array $params
     * @return array
     */
    public function getAll($params = [])
    {
        $ctaArr[] = \app\models\CallToAction::getKeywordBasedCTA();  
        return $ctaArr;
    }
    
    /**
     * Get details of single
     * 
     * @param array $params
     * @return array
     */
    public function get($params = [])
    {
        return \app\models\CallToAction::getKeywordBasedCTA();
    }
    
    public function getKeywordBasedCTA($keywords)
    {
        return $this->get();
    }

    public function getBlogCTA($keywords = NULL)
    {
        return $this->getAll();
    }

}
