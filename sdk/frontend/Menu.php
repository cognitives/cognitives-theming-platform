<?php

namespace sdk\frontend;

use Yii;
/**
 * Description of Menu
 *
 * @author Ashish
 */
class Menu extends \sdk\base\Menu
{

    public function get($params = [])
    {
        $menuData = \app\models\Menu::getMenu();
        $returnArr = []; 

        if(isset($params['menuType']) && !empty('menuType')){
            foreach ( $menuData as $menu) {
                if($menu['menuType'] == $params['menuType']){
                    $returnArr[] = $menu;
                }
            }
            return $returnArr;
        }
       return $menuData;
    }
    
    public function getSocialLinks()
    {
        $params['menuType'] = \app\models\Menu::MENU_TYPE_SOCIALLINK;
        return $this->get($params);
    }
    
    public function getMenu($name = 'Default')
    {
        $params['menuType'] = \app\models\Menu::MENU_TYPE_HEADER;
        return $this->get($params);
    }

    public function getHeaderMenu($name = 'Default')
    {
        $params['menuType'] = \app\models\Menu::MENU_TYPE_HEADER;
        return $this->get($params);
    }
    
    public function getFooterMenu($name = 'Default')
    {
        $params['menuType'] = \app\models\Menu::MENU_TYPE_FOOTER;
        return $this->get($params);
    }
}
