<?php
namespace sdk\frontend;

use Yii;

class Network
{

    public static $height = 60;
    public static $width = 600;
    public function getNetworkTitle()
    {
        $networkData = \app\models\Network::getNetworkData();
        return $networkData['title'];
    }

    public function getNetworkData()
    {
        $networkData = \app\models\Network::getNetworkData();
        if(!empty($networkData)) {
            $networkData['defaultBlogUrl']='/' ;
            $networkData['networkLogoLinkUrl']='/';
            $networkData['templatePath'] = '/themes/'.\Yii::$app->params['themeDir'];
        }
        return $networkData;
    }

    public function isNetworkClosed($network)
    {
        return FALSE;
    }

    public function getNetworkSections($params = [])
    {
        $networkSections = \app\models\Blog::getBlogList(['type' => 'section']);
        return $networkSections;
    }
    public static function getStructuredPublisher()
    {
        $structured =[];
        $network = \app\models\Network::getNetworkData();

        if(!empty($network)){
            $structured = [
                'name' =>$network['title'],
                'url' => $network['logoMedia']['path'],
                'width' => self::$width,
                'height' => self::$height,
            ];
        }
        return $structured;
    }
    
    public function getCaptchaSiteKey()
    {
        return FALSE;
    }
    public function getNetworkOwner($networkId = NULL)
    {
        $users = \app\models\User::getUsers('users');
        if(!empty($users)){
            $user=$users[0];
            return [
                'id' => $user['id'],
                'guid' => $user['guid'],
                'name' => $user['firstname'] . ' ' . $user['lastname'],
                'username' => $user['username'],
                'email' => $user['email'],
                'bio' => $user['bio'],
                'display_name' => $user['firstname'],
                'media' => [
                    'id' => $user['cloudinary_public_id'],
                    'cloudName' => $user['cloudinary_cloud_name'],
                    'path' => $user['profileImageURL']
                ]
            ];
        }
        return [];
    }

    public function getThemeConfig($array = true)
    {
        return false; 
    }

}
