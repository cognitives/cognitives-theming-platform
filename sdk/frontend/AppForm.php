<?php

namespace sdk\frontend;

/**
 * Description of AppForm
 *
 * @author Pawan Kumar
 */
class AppForm
{
    public static function input($params)
    {
        $type = 'text';
        if (isset($params['type']) && !empty($params['type'])) {
            $type = $params['type'];
            unset($params['type']);
        }
        $formName = '';
        if (isset($params['name']) && !empty($params['name'])) {
            $formName = $params['name'];
             unset($params['name']);
        }
        
        $formValue = '';
        if (isset($params['value']) && !empty($params['value'])) {
            $formValue = $params['value'];
            unset($params['value']);
        }

        return \yii\helpers\Html::input($type, $formName, $formValue, $params);
    }

    public static function captcha($params)
    {
        $preOptions = [
            'captchaAction' => \yii\helpers\Url::toRoute('auth/captcha'),
            'name' => 'captcha',
            'options' => $params
        ];
        return \yii\captcha\Captcha::widget($preOptions);
    }
    
    public static function button($params)
    {
        $label = 'Submit';
        if (isset($params['label']) && !empty($params['label'])) {
            $label = $params['label'];
            unset($params['label']);
        }
       return \yii\helpers\Html::button($label, $params);
    }
}
