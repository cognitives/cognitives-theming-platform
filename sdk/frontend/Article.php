<?php

namespace sdk\frontend;

class Article
{
    public $articleModelObj;
    public $networkSdkObj;
    public $metaDetails;
    
    /**
     * Pass article guid and get article preview url
     * @param array $params
     * @return string Url
     * @throws \yii\base\InvalidParamException
     */
    public function getArticleUrl($params = [])
    {
        if (!isset($params['guid']) || empty($params['guid'])) {
            throw new \yii\base\InvalidParamException("Article GUID is a required field.");
        }
        
        $articleUrl = \app\models\Article::getArticleUrl($params);
        
        return $articleUrl;
    }
    
    
    public function getEditArticleUrl($params = [])
    {
        return $this->getEditUrl($params);
    }

    public function getEditUrl($params = [])
    {
        if (!isset($params['guid']) || empty($params['guid'])) {
            throw new \yii\base\InvalidParamException("Article GUID is a required field.");
        }
        
        return '/article/edit?guid=' . $params['guid']; 
    }
    
    public function get($params = [])
    {
        if(isset($params['slug']) && !empty($params['slug'])){
            $articles = \app\models\Article::getArticleBySlug($params['slug']);
        }else{
            $articles = \app\models\Article::getArticle($params['articleGuid']);
        }        
        return $articles;
    }
    
    public function getAll($params = [])
    {
        if(!isset($params['limit'])) {
            $params['limit'] = 10;
        }
        $articles = \app\models\Article::getPopularArticles($params['limit']);
        
        return $articles;
    }
    
    /**
     * 
     * @param integer $articleId
     * @param array $params
     */
    public function getArticle($articleId, $params = [])
    {
        if (isset($articleId) && is_int($articleId) && $articleId <= 0) {
           throw new \yii\base\InvalidParamException("Invalid Article ID passed.");
        }
        
        $params['articleGuid'] = $articleId;
        
        return $this->get($params);
    }

    //checkUserArticleEditAccess
    public function getUserHasArticleEditAccess($articleId, $userId)
    {
        return TRUE;
    }
    
    /**
     * 
     * @param array $params
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getPopularArticles($params = [])
    {  
        $feed = \app\models\BlogFeed::getBlogFeed(['is_default' => true]);
        $limit = (isset($params['limit']) && $params['limit'] > 0) ? $params['limit'] : 3;
        $popular  = array_slice($feed, 0, $limit); 
        return $popular;
    }
    
    public function getRecentArticles($params = [])
    {
        return $this->getAll($params);
    }
    
    public function getCountUserArticles($userId)
    {
        if (is_integer($userId) && (int) $userId <= 0) {
            throw new \yii\web\HttpException('Invalid User ID passed');
        }
        
        $articleAQ = \app\models\Article::getPopularArticles(10);
        return count($articleAQ);
    }


    public function getMedia($articleGuid)
    {
        if (is_int($articleGuid) && $articleGuid <= 0) {
            throw new \yii\web\HttpException(500,'Invalid Article ID');
        }
        
        $articleMedia = \app\models\Article::getArticleMedia($articleGuid);
        
        return $articleMedia;
    }

    /**
     * Following functions are moved to SocialFeed, so now on following functions are deprecated
     */
    public function socialArticleSourceTwitter()
    {
        return \app\models\SocialFeed::SOURCE_TWITTER;
    }

    public function socialArticleSourceFacebook()
    {
        return \app\models\SocialFeed::SOURCE_FACEBOOK;
    }

    public function socialArticleSourceInstagram()
    {
        return \app\models\SocialFeed::SOURCE_INSTAGRAM;
    }

    public function socialArticleSourceYoutube()
    {
        return \app\models\SocialFeed::SOURCE_YOUTUBE;
    }

    public function socialArticleSourceVimeo()
    {
        return \app\models\SocialFeed::SOURCE_VIMEO;
    }

    public function socialArticleSourceGoogle()
    {
        return \app\models\SocialFeed::SOURCE_GOOGLE;
    }

    public function socialArticleSourcePinterest()
    {
        return \app\models\SocialFeed::SOURCE_PINTEREST;
    }
    public function getArticleBySlug($slug, $params = [])
    {
        if (empty($slug)) {
            throw new \components\exceptions\AppException(\Yii::t('app', 'missing.args.error', ['title' => 'Article Slug']), \components\exceptions\HttpStatus::HTTP_NOT_FOUND);
        }
        
        $params['slug'] = $slug;
        $article = $this->get($params); 
        
        if (!empty($article) && isset($article['guid'])) {
            $article['editUrl'] = $this->getEditArticleUrl(['guid' => $article['guid']]);
        }
                
        return $article;
    }

    public function getRelatedArticles($articleId, $params = [])
    {
        $articleModel = $this->getArticle($articleId);
        if ($articleModel == NULL) {
            return [];
        }

        if ($articleModel['relatedType'] == \app\models\Article::RELATED_TYPE_ARTICLE || (isset($params['relatedType']) && $params['relatedType'] == \app\models\Article::RELATED_TYPE_ARTICLE)) {
            return \app\models\Article::getRecentArticle(); 
        }

        else if ($articleModel['relatedType'] == \app\models\Article::RELATED_TYPE_TAGS || (isset($params['relatedType']) && $params['relatedType'] == \app\models\Article::RELATED_TYPE_TAGS)) {
            return \app\models\Article::getRecentArticle(); 
        }
        return \app\models\Article::getPopularArticles();
    }

    public function getLinkedCTA($params = [])
    {
        // return (new CallToAction())->getLinkedArticle($article['id]);
        return (new CallToAction())->getAll();
    }

    public function getLinkedContentBlock($params = [])
    {
        return (new ContentBlock())->getAll($params);
    }

    public static function getStructuredArticle($hasMedia, $media, $width, $height)
    {
        $structured = FALSE;
        if ($hasMedia) {
            $url = \sdk\frontend\Media::getMediaUrl($media, $width, $height, ['crop' => 'fill', 'gravity' => 'faces']);
            $structured = [
                'url' => $url,
                'height' => $height,
                'width' => $width,
            ];
        }

        return $structured;
    }
    
    public static function hasArticleVideoMedia($articleId)
    {
        return \app\models\Article::hasVideoMedia($articleId);

    }
    
    public function getArticleVideoMedia($articleId)
    {
        $articleMedia = [];
        $medias = \app\models\Article::videoMedia($articleId);
        
        if(count($medias) > 0) {
            foreach($medias as $media){
                $articleMedia = [
                    'id'        => $media['cloudinary_public_id'],
                    'videoId'   => isset($media["videoId"]) ? $media["videoId"] : NULL,
                    'cloudName' => $media['cloudinary_cloud_name'],
                    'path'      => $media['cdn_path'],
                    'type'      => 'video',
                    'source'    => isset($media['source']) ? $media['source'] : NULL,
                ];
            }

        }
        
        return $articleMedia;
    }


}
