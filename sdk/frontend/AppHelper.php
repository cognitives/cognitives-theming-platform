<?php

namespace sdk\frontend;

/**
 * Description of AppHelper
 *
 * @author Aman Dhaliwal
 */
class AppHelper extends \sdk\base\AppHelper
{
    public static function getRequest($params)
    {
        return \Yii::$app->request->get($params);
    }

    public static function getLoginUrl()
    {
        return \Yii::$app->urlManager->createUrl(['auth/login']);
    }
    
    public static function getResetPasswordUrl()
    {
        return \Yii::$app->urlManager->createUrl(['auth/reset-password']);
    }


    public static function getSignupUrl()
    {
        return \Yii::$app->urlManager->createUrl(['auth/signup']);
    }
    
    public static function getLogoffUrl()
    {
        return \Yii::$app->urlManager->createUrl(['auth/logoff']);
    }
    
    public static function getBaseUrl()
    {
        return \yii\helpers\Url::base(TRUE);
    }
    
    public static function getHomeUrl()
    {
        return \yii\helpers\Url::home(TRUE);
    }
    
    public static function getFormattedDate($date)
    {
        return \Yii::$app->formatter->asDate($date, 'yyyy-MM-dd');
    }
    
    public function getNetworkName()
    {
       return \Yii::t('app', 'network.name');
    }
    
    public static function getAssetsSDKPath()
    {
        return \Yii::$app->params['staticHttpPath'] . '/sdk';
    }
	
	public static function getAssetsStaticPath()
    {
        return \Yii::$app->params['staticHttpPath'];
    }
    
    public static function getForgotPasswordUrl()
    {
        return \Yii::$app->urlManager->createUrl(['auth/forgot-password']);
    }
    
    public static function getEditProfileUrl()
    {
        return \Yii::$app->urlManager->createUrl(['user/edit-profile']);
    }
    
    public static function getSocialTwitterUrl($login = false)
    {
        return \Yii::$app->urlManager->createUrl(['auth/auth', 'authclient' => 'twitter', 'login' => $login]);
    }
    
    public static function getSocialFacebookUrl($login = false)
    {
        return \Yii::$app->urlManager->createUrl(['auth/auth', 'authclient' => 'facebook', 'login' => $login]);
    }
    
    public static function getSocialLinkedinUrl($login = false)
    {
        return \Yii::$app->urlManager->createUrl(['auth/auth', 'authclient' => 'linkedin', 'login' => $login]);
    }
    public static function getSocialGooglePlusUrl($login = false)
    {
        return \Yii::$app->urlManager->createUrl(['auth/auth', 'authclient' => 'google', 'login' => $login]);
    }
    
    public static function getContactFormUrl()
    {
        return \Yii::$app->urlManager->createUrl(['home/contact-us']);
    }
    
    public static function getServerUrl()
    {
        $http = (\Yii::$app->request->isSecureConnection)? 'https://' : 'http://';
        return $http.''.\Yii::$app->request->serverName.''.\Yii::$app->request->url;
    }
    
    public static function getCurrentUrl()
    {
         return \Yii::$app->request->url;
    }


    public static function getUserProfileUrl($username)
    {
        if(isset($username) && empty($username)) {
            return FALSE;
        }
        
        return \Yii::$app->urlManager->createAbsoluteUrl(['profile/user-profile', 'name' => \yii\helpers\Html::encode($username)]);
    }

    public static function getDisqusToken($disqus)
    {
        $message = $hmac = $timestamp = '';

        if (!\Yii::$app->user->isGuest) {
            $userInfo = [
                "id" => \Yii::$app->user->id,
                "username" => \Yii::$app->user->getIdentity()->username,
                "email" => \Yii::$app->user->getIdentity()->email
            ];

            $timestamp = time();
            $message = base64_encode(json_encode($userInfo));
            $hmac = \components\Helper::dsqHmacsha1($message . ' ' . $timestamp, $disqus['apiKey']);
        }

        $token = "$message $hmac $timestamp";

        return [ 
            'token' => $token,
            'networkName' => \Yii::$app->network->data['title'],
        ];
    }
	
	public static function getApplicationEnv()
    {
        return \Yii::$app->params['applicationEnv'];
    }
    
    /**
     * Register Meta Tags in layouts
     * $params = [
     *      'name' => 'keywords'.
     *      'content' => 'fans, fashion'
     * ]
     * $params = [
     *      'property' => 'og:title'.
     *      'content' => 'title of this page'
     * ]
     * @param array $params
     * @return boolean
     */
    public static function registerMetaTag($params = [])
    {
        if (isset($params['name'])) {
            \Yii::$app->view->registerMetaTag(['name' => $params['name'], 'content' => isset($params['content']) ? $params['content'] : ''], $params['name']);
        }
        elseif (isset($params['property'])) {
            \Yii::$app->view->registerMetaTag(['property' => $params['property'], 'content' => isset($params['content']) ? $params['content'] : ''], $params['property']);
        }
        return TRUE;
    }
    
    public static function getFlashMessages()
    {
        return \Yii::$app->session->getAllFlashes();
    }
    
    public static function getLanguage()
    {
        return \Yii::$app->language;
    }
    
    public static function getCharset()
    {
        return \Yii::$app->charset;
    }
    
    public static function getErrorThemePath()
    {
        return \Yii::$app->params['themesS3HttpPath'] . '/error';
    }
    
    
    public static function getDateTimeBasedOnTimezone($dateTime, $dateType = 'SHORT')
    {
        try {
            $network = \Yii::$app->network->data;
            if (isset($network) && !empty($network['timezone'])) {
                $networkTimezone = $network['timezone'];
                $defaultTimezone = 'UTC';
                $dateObj = new \DateTime('@' . strtotime($dateTime), new \DateTimeZone($defaultTimezone));
                $dateObj->setTimezone(new \DateTimeZone($networkTimezone));
                switch ($dateType) {
                    CASE 'SHORT':
                        return $dateObj->format('M d Y');
                    CASE 'LONG':
                        return $dateObj->format('F d Y');
                    CASE 'META':
                        return $dateObj->format('Y-m-d');
                    default:
                        return $dateObj->format($dateType);
                }
            }
        }
        catch (\Exception $ex) {
            return $dateTime;
        }
    }
    
    public static function getDefaultTimezoneDateTime($timestamp, $format = null)
    {
        $timestamp = is_int($timestamp) ? $timestamp : strtotime($timestamp);
        $format = (is_null($format)) ? "Y-m-d H:i:s e" : $format;

        try {
            $defaultTimezone = 'UTC';
            $dateObj = new \DateTime('@'.$timestamp, new \DateTimeZone($defaultTimezone));
            return $dateObj->format($format);
        }
        catch (\Exception $ex) {
            return $timestamp;
        }
    }

    public static function getCurrentTimestampBasedOnTimezone()
    {
        $network = \Yii::$app->network->getData();
        $networkTimezone = isset($network['timezone'])?$network['timezone']:null;

        // get the current time as unix timestamp based on timezone of network
        $dateObj = new \DateTime( null, new \DateTimeZone('UTC'));
        if (!empty($networkTimezone)) {
            $dateObj->setTimezone(new \DateTimeZone($networkTimezone));
        }
        
        return ( $dateObj->getTimestamp() + $dateObj->getOffset() );
    }

    public static function getSecondsSincePublished($timestamp)
    {
        $current_time = self::getCurrentTimestampBasedOnTimezone();

        $timestamp = is_int($timestamp) ? $timestamp : strtotime($timestamp);

        return $current_time - $timestamp;

    }


    public static function getRelativeTime($timestamp)
    {
        $timestamp = is_int($timestamp) ? $timestamp : strtotime($timestamp);

        $current_time = self::getCurrentTimestampBasedOnTimezone();
        
        $time_ago = function($time, $rcs = 0) use ( &$time_ago, $current_time ) {
            
            $dif = $current_time - $time;

            $periods = array('second','minute','hour','day','week','month','year');
            $time_units = array(1,60,3600,86400,604800,2630880,31570560);

            $time_unit_count = sizeof($time_units) -1;
            while($time_unit_count >= 0) {
              $time_value = $dif / $time_units[$time_unit_count];

              if ($time_value > 1) {
                break;
              }

              $time_unit_count--;

            }
            
            if($time_unit_count < 0) {
                $time_unit_count = 0; 
            }

            $_tm = $current_time - ( $dif % $time_units[$time_unit_count] );
            $time_value = floor($time_value); 
            
            if ($time_value <> 1) {
                $periods[$time_unit_count] .='s'; 
            }
            
            $result = sprintf("%d %s ", $time_value, $periods[$time_unit_count]);
            
            if (($rcs == 1) && ($time_unit_count >= 1) && (($current_time - $_tm) > 0)) {
              $result .= $time_ago($_tm);
            }

            return $result;
        };

        return $time_ago($timestamp);
    }
	
	public static function getJsSdkPath()
    {
        return 'https://d31nhj1t453igc.cloudfront.net/sdk/sdk-v1.0.0.min.js';
    }

    public static function removeAllFlashes()
    {
        return true;
        // return \Yii::$app->session->removeAllFlashes();
    }

    public static function getParsedUrl()
    {
        return [];
    }

    public static function redirectUser($path)
    {
        \Yii::$app->response->redirect(\yii\helpers\Url::to($path));
        \Yii::$app->response->send();
        exit;
    }

    public static function isPostRequest()
    {
        return (Yii::$app->request->isPost) ? TRUE : FALSE;
    }
    
    public static function outputXMLResponse($response,$rootTag = NULL)
    {
        $response['format'] = 'xml';
        $response['rootTag'] = $rootTag;
        return $response;
    }

    public static function sendNetworkAdminEmail($subject, $message, $attachment = [], $toEmail = NULL)
    {
        return false;
    }
    
    public static function getRssFeedUrl()
    {
        return self::getHomeUrl().'feed/rss';
    }
    public function getBody()
    {
        $output = "";
        $networkData = \app\models\Network::getNetworkData();
        if(!empty($networkData)){
            if( isset($network['thirdPartyIntegrations']['googleTagManager']['noscript']) && !empty($network['thirdPartyIntegrations']['googleTagManager']['noscript'])) {
                $output .= $network['thirdPartyIntegrations']['googleTagManager']['noscript'];
            }
        }        
        return $output;
    }    
    public function getFooter()
    {
        $output = "";
        $networkData = \app\models\Network::getNetworkData();
        if(!empty($networkData)){
            if(isset($network['styling']['customFooter']) && !empty($network['styling']['customFooter'])) {
                $output .=<<<HTML
                        {$network['styling']['customFooter']}
HTML;
            }
        }
        return $output;
    }
    
    public function getHead()
    {
        $output = "";
        $output = "";
        $network = \app\models\Network::getNetworkData();
        if(!empty($network)){

            if(isset( $network['thirdPartyIntegrations']['googleTagManager'] ) && !empty($network['thirdPartyIntegrations']['googleTagManager']) ) {
                if(isset($network['thirdPartyIntegrations']['googleTagManager']['script']) && !empty($network['thirdPartyIntegrations']['googleTagManager']['script'])) {
                    $output .= $network['thirdPartyIntegrations']['googleTagManager']['script'];
                }
            }
            $primaryFont = (isset($network['styling']['fonts']['primaryFont']))?$network['styling']['fonts']['primaryFont'] : '';
            $secondaryFont = (isset($network['styling']['fonts']['secondaryFont']))?$network['styling']['fonts']['secondaryFont'] : '';
            if(!empty($primaryFont) && $primaryFont === $secondaryFont) {
                $output .= "<link href='https://fonts.googleapis.com/css?family={$primaryFont}' rel='stylesheet' type='text/css'>";
            }
            else {
                if(!empty($primaryFont)) {
                    $output .= "<link href='https://fonts.googleapis.com/css?family={$primaryFont}' rel='stylesheet' type='text/css' />\n";
                }
                if(!empty($secondaryFont)) {
                    $output .= "<link href='https://fonts.googleapis.com/css?family={$secondaryFont}' rel='stylesheet' type='text/css' />\n";
                }
            }
            $customCss = (isset($network['styling']['customStyleSheet']))?$network['styling']['customStyleSheet'] :'' ;
            $output .= "<style>";
            if(!empty($customCss)) {
                $output .= $customCss;
            }
            if(!empty($primaryFont)) {
                $priArr = explode(':', $primaryFont);
                $onlyFamily = array_shift($priArr);
                $family = str_replace('+', ' ', $onlyFamily);
                $output .=<<<HTML
                    body, .body, p {
                        font-family: "{$family}" !important;
                    }
HTML;
            }
            if(!empty($secondaryFont)) {
                $secArr = explode(':', $secondaryFont);
                $onlyFamily = array_shift($secArr);
                $family = str_replace('+', ' ', $onlyFamily);
                $output .=<<<HTML
    
                h1, h2, h3, h4, h5, a.header__navigation-link {
                    font-family: "{$family}" !important;
                }
HTML;
            }    

            $themeColors = (isset($network['styling']['colours']) ) ? $network['styling']['colours']:[] ;
            
            if( isset($themeColors['linkColor']) && !empty($themeColors['linkColor'])) {
                $output .=<<<HTML
                    a {
                        color: {$themeColors['linkColor']} !important;
                    }
HTML;
            }
            
            if(isset($themeColors['linkHoverColor']) && !empty($themeColors['linkHoverColor'])) {
                $output .=<<<HTML
                        
                    a:hover {
                        color: {$themeColors['linkHoverColor']} !important;
                    }
HTML;
            }
            if(isset($themeColors['headingColor']) && !empty($themeColors['headingColor'])) {
                $output .=<<<HTML
                        
                    h1, h2, h3, h4, h5 {
                        color: {$themeColors['headingColor']} !important;
                    }
HTML;
            }
            if(!empty($themeColors['navigationColor'])) {
                $output .=<<<HTML
                        
                    a.header__navigation-link {
                        color: {$themeColors['navigationColor']} !important;
                    }
HTML;
            }

            if(!empty($themeColors['navigationHoverColor'])) {
                $output .=<<<HTML
                        
                    a.header__navigation-link:hover {
                        color: {$themeColors['navigationHoverColor']} !important;
                    }
HTML;
            }
            if(!empty($themeColors['navigationActiveColor'])) {
                $output .=<<<HTML
                        
                    a.header__navigation-link.header__navigation-selected {
                        color: {$themeColors['navigationActiveColor']} !important;
                    }
HTML;
            }
            if(!empty($themeColors['navigationBgColor'])) {
                $output .=<<<HTML
                        
                    .header__navigation {
                        background-color: {$themeColors['navigationBgColor']} !important;
                    }
HTML;
            }
            if(!empty($themeColors['headerBgColor'])) {
                $output .=<<<HTML
                        
                    .header__heading {
                        background-color: {$themeColors['headerBgColor']} !important;
                    }
HTML;
            }
            if(!empty($themeColors['footerBgColor'])) {
                $output .=<<<HTML
                        
                    .footer {
                        background-color: {$themeColors['footerBgColor']} !important;
                    }
HTML;
            }
            if(!empty($themeColors['pageBgColor'])) {
                $output .=<<<HTML
                        
                    body, .body, .body > .section {
                        background-color: {$themeColors['pageBgColor']} !important;
                    }
                    .sub_header {
                        background-color: {$themeColors['pageBgColor']} !important;
                    }
HTML;
            }
            if(!empty($customCss)) {
                $output .= $customCss;
            }
            $output .= "</style>";
            $googleAnalyitcsId = '';
            if( isset($network['thirdPartyIntegrations']['googleAnalytics']['id']) && !empty($network['thirdPartyIntegrations']['googleAnalytics']['id'])) {
                $googleAnalyitcsId = $network['thirdPartyIntegrations']['googleAnalytics']['id'];
            }
            
            if(!empty($googleAnalyitcsId)) {
                $output .=<<<HTML
                <!-- Google Analytics -->
                    <script>
                        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
                        ga('create', '{$googleAnalyitcsId}', 'auto');
                        ga('send', 'pageview');
                    </script>
                <!-- End Google Analytics -->
HTML;
            }
            
            if(isset($network['styling']['customHeader']) && !empty($network['styling']['customHeader'])) {
                $output .=<<<HTML
                        {$network['styling']['customHeader']}
HTML;
            }
            
        }// network
        return $output;
    }
}
