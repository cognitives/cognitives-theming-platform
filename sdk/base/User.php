<?php

namespace sdk\base;

/**
 * Description of User
 *
 * @author Aman Dhaliwal
 */
class User
{

    public static function getIsUserLoggedIn()
    {
        return !\Yii::$app->user->isGuest;
    }

    public function isAdminUser()
    {
        if (\Yii::$app->session->has('userNetworkRole')) {
            $userNetworkRole = \Yii::$app->session->get('userNetworkRole');
            if (!in_array('endUser', $userNetworkRole)) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
