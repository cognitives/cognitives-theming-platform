<?php

namespace sdk\base;

class AppHelper
{
    public static function getCsrfToken()
    {
        return \Yii::$app->request->csrfToken;
    }
    
    public static function buildUrl($url, $params = [])
    {
        $urlParams = (isset($params) && $params > 0) ? $params : '';
        return \Yii::$app->urlManager->createUrl(\yii\helpers\ArrayHelper::merge([$url], $urlParams));
    }
    
    public static function printArray($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }
}
